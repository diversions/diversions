from __future__ import print_function
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from time import sleep
from selenium.common.exceptions import NoSuchElementException, NoSuchWindowException
from urllib2 import urlopen
import sys, json, os
from argparse import ArgumentParser


count=0
def image_path (x):
    x = x.replace(" ", "_").lower()
    x = x+".jpg"
    return x

def log (*msg):
    print (*msg, file=sys.stderr)

ap = ArgumentParser("MRAH scaper")
ap.add_argument("--starturl", default="http://carmentis.be")
ap.add_argument("--sleeptime", type=float, default=None, help="sleeptime")
args = ap.parse_args()

sleeptime = args.sleeptime
log("Opening browser...")

browser = webdriver.Firefox()
browser.get(args.starturl)
log("Perform a search and select detail mode, then press enter to start scraping items... (Ctrl-c to cancel)")
raw_input()

browser.implicitly_wait(10)

while True:
    count += 1
    browser.save_screenshot('screenie'+str(count)+'.png')

    try:
        next = browser.find_element_by_css_selector('#pageSetEntries-nextSet a')
        log(next)
    except NoSuchElementException as e:
        log("no element")
        pass
    if next == None:
        log("END OF LIST")
        break
    log("NEXT")
    log(next)
    next.click()
    if sleeptime:
        sleep(sleeptime)

    log("output {0} items".format(count))

browser.quit()
