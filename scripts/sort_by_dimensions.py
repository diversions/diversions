# dimensions parser
from __future__ import print_function
import sys, json, re
from argparse import ArgumentParser

ap = ArgumentParser("")
ap.add_argument("--indent", type=int, default=None)
args = ap.parse_args()


def parse_dimensions (d):
    lpat = re.compile(r"Length: (?P<x>\d+(?:,\d+)?) cm", re.I)
    diapat = re.compile(r"Diameter: (?P<x>\d+(?:,\d+)?) cm", re.I)
    hpat = re.compile(r"Height: (?P<x>\d+(?:,\d+)?) cm", re.I)
    wpat = re.compile(r"Width: (?P<x>\d+(?:,\d+)?) cm", re.I)
    dpat = re.compile(r"Depth: (?P<x>\d+(?:,\d+)?) cm", re.I)
    weightpat = re.compile(r"Weight: (?P<x>\d+(?:,\d+)?) g", re.I)

    length, diameter, height, width, depth, weight = None, None, None, None, None, None

    values = []

    m = lpat.search(d)
    if m:
        length = float(m.groupdict().get('x').replace(',', '.'))
        values.append(length)
    m = diapat.search(d)
    if m:
        diameter = float(m.groupdict().get('x').replace(',', '.'))
        values.append(diameter)
    m = hpat.search(d)
    if m:
        height = float(m.groupdict().get('x').replace(',', '.'))
        values.append(height)
    m = wpat.search(d)
    if m:
        width = float(m.groupdict().get('x').replace(',', '.'))
        values.append(width)
    m = dpat.search(d)
    if m:
        depth = float(m.groupdict().get('x').replace(',', '.'))
        values.append(depth)

    m = weightpat.search(d)
    if m:
        weight = float(m.groupdict().get('x').replace(',', '.'))
    
    values.sort(reverse=True)
    return {
        'sorted': values,
        'length': length,
        'diameter': diameter,
        'height': height,
        'width': width,
        'depth': depth,
        'weight': weight,
    }


def process_csv (f):
    from csv import DictReader
    items = DictReader(f)
    decorated = []
    for item in items:
        # item = json.loads(line)
        if 'dimensions' in item:
            d = parse_dimensions(item['dimensions'])
            if (d['length'], d['diameter'], d['height'], d['width'], d['depth'], d['weight']) == (None, None, None, None, None, None):
                print ("NO VALUE FOUND IN", d, (d['length'], d['diameter'], d['height'], d['width'], d['depth']), file=sys.stderr)
            else:
                key = d['sorted']
                item['sort'] = key
                labels = item['sortlabels'] = []
                if d['length']:
                    labels.append("Length: {0:0.0f} cm".format(d['length']))
                if d['width']:
                    labels.append("Width: {0:0.0f} cm".format(d['width']))
                if d['height']:
                    labels.append("Height: {0:0.0f} cm".format(d['height']))
                if d['depth']:
                    labels.append("Depth: {0:0.0f} cm".format(d['depth']))
                if d['diameter']:
                    labels.append("Diameter: {0:0.0f} cm".format(d['diameter']))
                if d['weight']:
                    labels.append("Weight: {0:0.0f} g".format(d['weight']))
                decorated.append((key, item))
        else:
            print ("NO DIMENSIONS", item['image'], file=sys.stderr)
            # print '---'

    decorated.sort()
    # output undecorated item
    # print (json.dumps([x for x in decorated], indent=2))
    print (json.dumps([item for _, item in decorated], indent=2))


if __name__ == "__main__":

    process_csv(sys.stdin)
