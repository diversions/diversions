<header class="editor-title"><h2>Titre du menu</h2>
    <div class="editor-info texte mini">A propos de ce menu, un peu d'information en petite typo.</div>
</header>
<form method="post">
    <div class="form-part open" id="edit-actions">
        <header class="form-part-head">
            <h3><a href="javascript:form_showhide('#edit-actions');"><i class="fa fa-chevron-right fa-fw fa-rotate-90"></i> Action</h3></a>
        </header>
        <div class="form-part-content">
            Contenu du form part
        </div>
    </div>

    <div class="form-part close" id="edit-items">
        <header class="form-part-head">
            <h3><a href="javascript:form_showhide('#edit-items');"><i class="fa fa-chevron-right fa-fw"></i> Items <span>(0)</span></h3></a>
        </header>
        <div class="form-part-content">
            Contenu du form part
        </div>
    </div>
    
</form>
