<?php
include("custom/config.php");
include($GLOBALS['root']."includes/inc-functions.php");
fix_exists_dir($GLOBALS['root'].$GLOBALS['dossier_cache']);
$fichier=$GLOBALS['root'].$GLOBALS['dossier_cache']."collection.html";
if(file_exists($fichier)){
    echo file_get_contents($fichier);
} else {
    $page="";
    $liste = glob($GLOBALS['dossier_xml']."*.xml", GLOB_BRACE);
    

    if(is_array($liste)){
        foreach($liste as $elem){ 
            $item = read_xml($elem);
            $page.=affiche_vignette($item);
        }
        file_put_contents($fichier, $page);
        echo $page;
    }

}
?>