<?php
$logins=array(
    "stephane"=> array(
        "nom"=>"Stéphane",
        "pwd"=>"valise"
    ),
);

$GLOBALS=array(
    'root' => str_replace("custom","",dirname(__FILE__)),
    'definition_item' => "custom/elements/item.xml",
    'definition_selection' => "custom/elements/selection.xml",
    'titre' => "XML Carmentis",
    'dossier_pages' => "custom/pages/",
    'dossier_xml' => "content/xml/",
    'dossier_images' => "content/img/",
    'dossier_vignettes' => "content/vignettes/",
    'dossier_selections' => "content/selections/",
    'dossier_cache' => "content/cache/",
    'docs_allowed' => "jpg,png,gif,tif,doc,pdf,zip,mp4,mp3",
    'icone' => array(
        "Sons" => "<i class='fa fa-file-audio-o'></i>",
        "Vidéos" => "<i class='fa fa-file-video-o'></i>",
        "Download" => "<i class='fa fa-file-o'></i>"
    )
)

?>
