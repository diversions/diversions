<!doctype html>
<html class="no-js" lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $GLOBALS['titre']; ?></title>
        <meta name="description" content="XML collection pour <?php echo $GLOBALS['titre']; ?>">
        <meta name="generator" content="XML collection par Stéphane Noël">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/jquery.fancybox.css">
        <link rel="stylesheet" href="css/font-awesome/font-awesome.min.css" charset="utf-8">
        <link rel="stylesheet" href="css/roboto/stylesheet.css" charset="utf-8">
        <link rel="stylesheet" href="css/font-awesome/font-awesome.min.css" charset="utf-8">

        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/jquery.fancybox.pack.js"></script>
        <script src="js/jquery.quicksearch.js"></script>

        <script src="js/sortChildren.min.js"></script>

        <script src="js/main.js"></script>
    </head>
    <body class="fullwidth">
        <!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

        <div id="wrapper">
            <header id="main-header" class="clearfix">
                <h1 class="colonne full nobottom"><a href="./"><?php echo $GLOBALS['titre']; ?></a></h1>

                <div id="main-nav" class="colonne full padlat clearfix">
                    <nav class="breadcrumb colonne demi nopad">
                        <?php
                        $liste = glob($GLOBALS['root'].$GLOBALS['dossier_pages']."*.xml", GLOB_BRACE);
                        $c=0;
                        foreach($liste as $item){
                            $contenu=contenu_xml($item);
                            if($c !=0) echo " / ";
                            echo "<a href='load-content.php' class='ajax-load' data-load='content.php?page=".$item."' data-class='".stripAccents($contenu['titre'])."' data-clic-action='one' data-cas='page'>".$contenu['titre']."</a>";
                            $c++;
                        }
                        ?>
                        / <a href="#" class="ajax-load" data-load="ajax/ajax-load-selections.php" data-clic-action="one" data-cas="form">Sélections</a>

                    </nav>

                    <nav class="breadcrumb colonne demi nopad adminnav">
                        <?php affiche_menu_admin(); ?>
                    </nav>
                </div>
                <div class="sep"></div>
                <nav id="display-tools" class="breadcrumb colonne demi padlat">
                    <span class="icon-group">
                        <a href="#" title="Mosaïque" class="display-liste on" data-id="mosaic"><i class="fa fa-th"></i></a>
                        <a href="#" title="Grande liste" class="display-liste" data-id="liste"><i class="fa fa-th-list"></i></a>
                        <a href="#" title="Petite liste" class="display-liste" data-id="liste-small"><i class="fa fa-list"></i></a>
                        <a href="#" title="Liste horizontale" class="display-liste" data-id="wide"><i class="fa fa-pause"></i></a>
                        <a href="javascript:cache_colonne_display();" id="bouton-fullwidth"><i class="fa fa-arrows-h"></i></a>
                    </span>
                    <span class="icon-group">
                    Trier <a href="javascript:reorder('id');" title="Par ID"><i class="fa fa-times"></i></a> <a href="javascript:reorder('down');" title="Par titre"><i class="fa fa-arrow-down"></i></a><a href="javascript:reorder('up');" title="Par titre inverse"><i class="fa fa-arrow-up"></i></a>
                    </span>
                    <span class="icon-group">
                    <a href="javascript:show_search();" class="searchlink">Chercher</a>
                    <input class="small-input" type="text" name="search" placeholder="enter a few words">
                    </span>
                </nav>
                <nav id="display-statut" class="colonne demi padlat">Affichage actuel : <span><a class="tool-button small" href="javascript:deselect_all_items()">Collection complète</a></span></nav>
            </header>
