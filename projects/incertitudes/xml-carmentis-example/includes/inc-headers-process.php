<?php
if(isset($_POST['login']) && isset($_POST['pwd'])) {
    if(isset($logins[$_POST['login']])){
        $i=$logins[$_POST['login']];

        if($i['pwd']==$_POST['pwd']){
            // on la démarre :)
            $_SESSION['login'] = $_POST['login'];
            $_SESSION['pwd'] = $_POST['pwd'];
            $_SESSION['nom']=$logins[$_POST['login']]['nom'];
        } else {
            echo "Mauvais login ou mot de passe";
        }	
    } else {
        echo "Login/mot de passe non reconnu";
    }
}
if(isset($_POST['ajouter_item'])){
    unset($_POST['ajouter_item']);
    // traiter l'import d'images :
    // uploader puis ajouter le lien dans le champ xml correspondant
    // prevoir de multiples fichiers, donc par fonction
    if(isset($_FILES)){        
        foreach($_FILES as $nom => $file){
            $lien=traiter_upload_doc($file);
            if($lien != -1){
                if($nom=="file"){
                    if(!isset($_POST['file'])){
                        $_POST['file']=array();
                    }
                    $_POST['file']=array_merge($_POST['file'], $lien);
                } else {
                    $_POST[$nom]=$lien[0];
                }
            }
        }
    }
    echo write_xml($_POST,$ecrase=false,"item");
}
if(isset($_POST['ajouter_selection'])){
    unset($_POST['ajouter_selection']);
    // traiter l'import d'images :
    // uploader puis ajouter le lien dans le champ xml correspondant
    // prevoir de multiples fichiers, donc par fonction
    if(isset($_POST['ecrase'])){
        if($_POST['ecrase'] =='false'){
            $ecrase=false;
        } else {
            $ecrase=true;
        }
    } else {
        $ecrase=false;
    }
    unset($_POST['ecrase']);
    echo write_xml($_POST,$ecrase,"selection");
}
if(isset($_POST['update_item'])){
    unset($_POST['update_item']);
    print_r($_FILES);
    if(isset($_FILES)){    
        foreach($_FILES as $nom => $file){
            $lien=traiter_upload_doc($file);
            if($lien != -1){
                if($nom=="file"){
                    if(!isset($_POST['file'])){
                        $_POST['file']=$lien;
                    } else {
                        $_POST['file']=array_merge($_POST['file'], $lien);
                    }
                } else {
                    $_POST[$nom]=$lien[0];
                }
            }

        }
    }
    echo write_xml($_POST,$ecrase=true,"item"); 
}
if(isset($_POST['import_images'])){
    unset($_POST['import_images']);
    
    fix_exists_dir($GLOBALS['root'].$GLOBALS['dossier_images']);

    $base=$_POST;
    $base['date_entree'] = date("Y-m-d H:i:s");

    $liste= glob($GLOBALS['root']."upload/*.{jpg,png,gif,JPG}", GLOB_BRACE);
    $num=0;
    $retour="";

    foreach($liste as $image){
        // voir si le nom est déja utilisé dans le dossier img. 
        // si oui, donner un code en plus
        // deplacer le fichier
        // creer un xml avec le nom comme titre
        $nom_image=fichier_seul($image);

        if(file_exists($GLOBALS['root'].$GLOBALS['dossier_images'].$nom_image)){
            $ad= hash('adler32', date("Y-m-d H:i:s"));
            $nom_image=$ad.$nom_image;    // ajouter ça avant l'extension plus tard
        }
        rename($image, $GLOBALS['root'].$GLOBALS['dossier_images'].$nom_image);
        $base['illustration']=$GLOBALS['dossier_images'].$nom_image;

        if(!isset($_POST['title'])){
            $base['title']=pathinfo($nom_image, PATHINFO_FILENAME);
        } else {
            $base['title']=$_POST['title']."_".hash('adler32', date("Y-m-d H:i:s").$num);

        }
        $l=write_xml($base,false);
        $item=read_xml($l);
        $retour.=affiche_vignette($item);

        $num++;   
    }
    $message="Importation de $num fiches.";
    echo $retour;

}
if(isset($_POST['update_items'])){
   // print_r($_POST);

    $items=$_POST['item'];
    unset($_POST['item']);
    unset($_POST['update_items']);

    if(isset($_FILES)){    
        foreach($_FILES as $nom => $file){
            $lien=traiter_upload_doc($file);
            if($lien != -1){
                if($nom=="file"){
                    if(!isset($_POST['file'])){
                        $_POST['file']=$lien;
                    } else {
                        $_POST['file']=array_merge($_POST['file'], $lien);
                    }
                } else {
                    $_POST[$nom]=$lien[0];
                }
            }

        }
    }

    foreach($items as $item){
        if(file_exists( $item)){
            update_xml($item,$_POST,"item");
        }
    }
}
if(isset($_POST['genere_vignette'])){
    $url=$_POST['url'];
    $c=read_xml($url);
    echo affiche_vignette($c);
}
if(isset($_POST['supprimer_item'])){
    $item=$_POST['item'];
    // rechercher les documents associés et les supprimer

    unlink($GLOBALS['root'].$item);
    remove_cache("collection.html");
}
if(isset($_POST['supprimer_selection'])){
    $selection=$_POST['selection'];
    // rechercher les documents associés et les supprimer
    unlink($GLOBALS['root'].$selection);
}
if(isset($_POST['rotation_image'])){
    ini_set('memory_limit', '256M');
    $filename = $GLOBALS['root'].$_POST['url'];
    $degrees=$_POST['angle'];
    traiter_rotation_image($filename, $degrees);
}
if(isset($_POST['rotate_all'])){
    $liste_items=$_POST['item'];
    $degrees=$_POST['rotate_all'];

    foreach($liste_items as $item){
        $c=read_xml($item);
        traiter_rotation_image($GLOBALS['root'].$c['illustration'], $degrees);
    }    
}
if(isset($_POST['delete_all'])){
    $liste_items=$_POST['item'];

    foreach($liste_items as $item){
        unlink($GLOBALS['root'].$item);
        remove_cache("collection.html");
    }    
}
if(isset($_POST['update_all'])){
    $liste_items=$_POST['item'];
    $post_info=$_POST;
    unset($post_info['item']);
    unset($post_info['update_all']);
    unset($post_info['date_entree']);


    // virer les champs vides
    foreach($post_info as $champ => $valeur){
        if(trim($valeur)==""){
            echo "je supprime ".$champ;
            unset($post_info[$champ]);   
        }
    }


    //traitement des pieces jointes
    if(isset($_FILES)){    
        foreach($_FILES as $nom => $file){
            $lien=traiter_upload_doc($file);
            if($lien != -1){
                if($nom=="file"){
                    if(!isset($_POST['file'])){
                        $post_info['file']=$lien;
                    } else {
                        $post_info['file']=array_merge($post_info['file'], $lien);
                    }
                } else {
                    $post_info[$nom]=$lien[0];
                }
            }

        }
    }


    foreach($liste_items as $item){
        // lire les champs d'origine du xml
        // remplacer les champs du xml
        $origin_xml=read_xml($item);
        $new_xml=array_merge($origin_xml,$post_info);

        $new_xml['url']=$item;
        echo write_xml($new_xml,true); 

    }
    remove_cache("collection.html");
}
?>