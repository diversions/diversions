<?php
session_start();
function _high($page){
    if($page==basename($_SERVER['PHP_SELF'])){ echo " class='on'"; }
}

function affiche_menu_admin(){
    if (!isset($_SESSION['login']) && !isset($_SESSION['pwd'])) {
        include("menus/form-login.php");
    } else {
        include("menus/admin-menu.php");   
    }
}

function fix_exists_dir($dir){
    if(!is_dir($dir)){
        mkdir($dir, 0777, true);
    }
}
// - - - fonction de lecture / ecriture du xml 
function read_xml($file){
    $contenu=json_decode(json_encode((array)simplexml_load_file($GLOBALS['root'].$file)),1);
    $contenu['url']=$file;
    return $contenu;
}

function contenu_xml($source){
    $structure=array();
    $document_xml = new DomDocument();
    $document_xml->load($source);

    $champs = $document_xml->getElementsByTagName('contenu')->item(0)->childNodes;
    foreach($champs as $champ){
        if ($champ->nodeType != XML_TEXT_NODE){ 
            $structure[$champ->nodeName]=$champ->nodeValue;
        }
    }
    return $structure;
}

// encode le xml sur base d'un array, la plupart du temps $_POST
function write_xml($contenu,$ecrase=false,$hometag="item"){
    if($hometag == "selection"){
        $dossier = $GLOBALS['root'].$GLOBALS['dossier_selections'];
        $dossier_local = $GLOBALS['dossier_selections'];
        $definition=$GLOBALS['root'].$GLOBALS['definition_selection'];
    }
    else {
        $dossier = $GLOBALS['root'].$GLOBALS['dossier_xml'];
        $dossier_local = $GLOBALS['dossier_xml'];
        $definition=$GLOBALS['root'].$GLOBALS['definition_item'];
    } 

    fix_exists_dir($dossier);

    if(isset($contenu['title'])){
        $nom=$contenu['title'];
    } else {
        $nom="file".hash('adler32', date("Y-m-d H:i:s"));
    }
    $nom=stripAccents($nom);
    
    $doc = new DOMDocument();
    $doc->formatOutput = true;
    $r = $doc->createElement( $hometag );
    $doc->appendChild( $r );

    $xml = simplexml_load_file($definition);
    // parcourir les champs du xml et ecrire dans l'ordre de celui-ci en encodant le type
    // le but est de pouvoir reconnaitre les texte, images et à l'avenir d'autre type
    // comme une liste d'item par exemple

    foreach($xml->children() as $champ) {
        $titre = $champ->attributes()->titre;
        $type = $champ->attributes()->type;
        $al = $champ->attributes()->allow;
        if($al){ // extension autorisées éventuelles
            // echo "il comporte un allow qui est ".$al."<br>";   
        }

        if(@count($champ->children())){
            // un noeud avec des enfants, on regarde le premier enfant
            $ch=$champ->children()->item[0];
            $ti = $ch->attributes()->titre;
            $ty = $ch->attributes()->type;
            // on encode les éléments correspondant à l'enfant
            $tag = $ch->getName();

            // passer en revue l'array de contenu
            if(isset($contenu[$tag])){
                $par = $doc->createElement( $champ->getName() );
                if(!is_array($contenu[$tag])){ // transformer en array si unique
                    $contenu[$tag]=array(0=>$contenu[$tag]);
                }
                foreach($contenu[$tag] as $e){
                    // if(!empty($contenu[$tag])){
                    $elem= $doc->createElement( $tag );
                    $elem->setAttribute( "type", $ty );
                    $elem->appendChild(
                        $doc->createTextNode( $e )
                    );
                    $par->appendChild( $elem );

                }
                unset($contenu[$tag]);
                // n'ajouter au xml que s'il y a quelque chose
                // if($r->children()[0]){
                $r->appendChild( $par );   
                // }
            }

        } else {
            // c'est un élément final, on encode suivant le tag
            $tag = $champ->getName();
            if(isset($contenu[$tag])){
                
                if(!empty($contenu[$tag])){

                    if(is_array($contenu[$tag])){
                        foreach($contenu[$tag] as $e){
                            $e=stripslashes($e);
                            
                            $elem= $doc->createElement( $tag );
                            $elem->setAttribute( "type", $type );
                            $elem->appendChild(
                                $doc->createTextNode( $e )
                            );
                            $r->appendChild( $elem );

                        }
                    } else {
                        $contenu[$tag]=stripslashes($contenu[$tag]);
                        $elem= $doc->createElement( $tag );
                        $elem->setAttribute( "type", $type );
                        $elem->appendChild(
                            $doc->createTextNode( $contenu[$tag] )
                        );
                        $r->appendChild( $elem );
                    }
                }
                unset($contenu[$tag]);
            }

        }
    }

    // traiter les champs restant
    foreach($contenu as $nom => $valeur){
        if(is_array($valeur)){
            foreach($valeur as $v){
                $elem= $doc->createElement( $nom );
                $elem->appendChild(
                    $doc->createTextNode( $v )
                );
                $r->appendChild( $elem );
            }

        } else {

            $elem= $doc->createElement( $nom );
            $elem->appendChild(
                $doc->createTextNode( $valeur )
            );
            $r->appendChild( $elem );
        }
    }

    if($ecrase==false ){
        if(file_exists($dossier.$nom.".xml")){
            $ad= hash('adler32', date("Y-m-d H:i:s"));
            $nom=$nom."_".$ad;
        }
        $doc->save($dossier.$nom.".xml");
        remove_cache("collection.html");
        return $dossier_local.$nom.".xml";
        if($hometag=='item'){
            remove_cache("collection.html");
        }

    } else if($ecrase==true){
        $nom = $contenu['url'];
        //unset($_POST['url']);
        $doc->save($GLOBALS['root'].$nom);

        if($hometag=='item'){
            remove_cache("collection.html");
        }
        return $nom;

    }
}
// - - - affichage des elements
function deployer_info($item=array()){
    echo "<ul class='file-'>";

    $xml = simplexml_load_file($GLOBALS['root'].$GLOBALS['definition_item']);
    foreach($xml->children() as $champ) {

        $titre = $champ->attributes()->titre;
        $type = $champ->attributes()->type;

        if(isset($item[$champ->getName()])){

            $tag = $champ->getName();
            $inner=$item[$champ->getName()];
            if(isset($inner)){

                if($type == "file"){
                    if(!is_array($inner)){
                        $inner=array($inner);
                    }
                    echo "<li class='display-$tag' title='$titre'>";
                    deployer_files($inner,$item['url'],0);
                    echo "</li>";

                }

                if(is_array($inner)){
                    // array, ne rien faire pour le moment

                } else {
                    if($type=="text" || $type=="select"){
                        echo "<li class='display-$tag' title='$titre'>".$inner."</li>";
                    }
                    if($type=="textarea"){
                        echo "<li class='display-$tag' title='$titre'>".$inner."</li>";
                    }
                    if($type=="image"){
                        echo "<li class='display-$tag' title='$titre'><img src='".donne_lien(_c($item,$champ->getName()),$l_vignette=600,$h_vignette=600,false)."' data-original='"._c($item,$champ->getName())."' class='adapt'></li>";
                    }
                }

            }
        }
    }
    echo "</ul>";

}

function affiche_vignette($item){
    $vignette="";
    $vignette.="<li class='item clearfix noload' data-url='".$item['url']."' onclick='sidebar_action(\"".$item['url']."\");'>\n";
    $vignette.="<div class='illu'><img src='".donne_lien(_c($item,'illustration'),$l_vignette=150,$h_vignette=150,true)."' class='adapt illustration'></div>\n";

    $vignette.="<div class='info-content'>\n";
    $vignette.="<div class='titre'>".$item['title']."</div>\n";
    $vignette.="<div class='descriptif'>\n";
    $vignette.= _c($item,'note');

    $vignette.=_c($item,'author',null,"<br><strong>Auteur : </strong>\n");
    $vignette.= _c($item,'publisher',null,"<br><strong>Maison d'édition : </strong>\n");
    $vignette.= _c($item,'isbn',null,"<br><strong>Isbn : </strong>\n");
    $vignette.= _c($item,'year',null,"<br><strong>Date : </strong>\n");
    $vignette.= "</div>\n";
    $vignette.= "</div>\n";

    $vignette.= "</li>\n";
    return $vignette;
}

function remove_cache($file="collection.html"){
    $l=$GLOBALS['root'].$GLOBALS['dossier_cache'].$file;
    if(file_exists($l)){
        unlink($l);
    }
}

// - - - fonction de test et de traitements d'info image

function fichier_seul($file){
    $file = preg_replace('/.*\//i', '', $file);
    return $file;	
}

function stripAccents($string){
    $str = htmlentities($string, ENT_NOQUOTES, 'UTF-8');
    $str = preg_replace('#\&([A-za-])(?:acute|cedil|circ|grave|ring|tilde|uml)\;#', '\1', $str);
    $str = strtolower(str_replace(" ","_",$str));
    return $str;
}

function _c($source,$valeur,$defaut=null,$titre=null){

    if(isset($source[$valeur]) && !empty($source[$valeur])){
        if(!is_array($source[$valeur])){
            if($titre){ $source[$valeur]= $titre.$source[$valeur]; }
            return htmlspecialchars($source[$valeur],ENT_QUOTES); 
        } else {
            // c'est un array
            // pour le moment ne rien envoyer
            return "";
        }
    } else {
        if($defaut){
            if($titre){ $defaut=$titre.$defaut; }
            return $defaut;
        } else {
            return "";   
        }
    }

}

// donne lien : reçoit une adresse de fichier et une taille, verifie si une vignette a cette taille existe et produit une version de cette image le cas échaant renvoie cette adresse
function donne_lien($file,$l_vignette=600,$h_vignette=600,$crop=true){
    // creer le dossier pour les images petites
    fix_exists_dir($GLOBALS['root'].$GLOBALS['dossier_vignettes']);

    if(!is_file($GLOBALS['root'].$file)){ 
        $file="img-default/default.jpg"; 
    }
    $ext_image=array('jpg','png','gif');
    $ext=strtolower(substr(strrchr($file,"."),1));
    $add="_".$l_vignette."_".$h_vignette;
    $nom=fichier_seul($file);
    $local_name=$GLOBALS['dossier_vignettes'].$nom.$add.".".$ext;

    // - - - image : traitement des icones
    if(in_array($ext,$ext_image)){
        // charger l'image brute
        // vérifier que l'image existe
        // sinon la créer
        if (!file_exists($GLOBALS['root'].$local_name)) {
            if(image_resize($GLOBALS['root'].$file, $GLOBALS['root'].$local_name, $l_vignette, $h_vignette, $crop)){
                return $local_name."?".filectime($GLOBALS['root'].$local_name);
            } else {
                return $file."?".filectime($GLOBALS['root'].$file);
            }
        } else {
            return $local_name."?".filectime($GLOBALS['root'].$local_name);   
        }

    } else {
        return "img_sys/error.jpg";   
    }
    // - - - - fin traitement des images   

}

function deployer_form_image($nom,$url_image,$url_item){
    echo "<img src='".donne_lien($url_image,$l_vignette=600,$h_vignette=600,false)."' data-original='".$url_image."' class='adapt'>";
    echo "<div class='tools-image item-tools' data-item='".$url_item."'>";
    echo"<a href='javascript:image_rotate(\"".$url_image."\",90,\"".$url_item."\");'><i class='fa fa-undo' class='icon-rotate'></i></a> <a href='javascript:image_rotate(\"".$url_image."\",-90,\"".$url_item."\");' class='icon-rotate'><i class='fa fa-repeat'></i></a> <a href='javascript:image_delete(\"".$url_image."\",\"".$url_item."\");' class='icon-delete carefull'><i class='fa fa-times'></i></a>";
    echo "<input type='hidden' name='".$nom."' value='".$url_image."'>";
    echo "</div>";
}

function image_resize($src, $dst, $width=500, $height=400, $crop=0){

    ini_set('memory_limit', '256M');
    if(!list($w, $h) = getimagesize($src)) {
        echo "image does not exists";
        return false;
    }

    $type = strtolower(substr(strrchr($src,"."),1));
    if($type == 'jpeg') $type = 'jpg';
    switch($type){
        case 'gif': $img = imagecreatefromgif($src); break;
        case 'jpg': $img = imagecreatefromjpeg($src); break;
        case 'png': $img = imagecreatefrompng($src); break;
        default : echo "image does not exists"; return false;
    }

    // resize
    if($crop){
        if($w < $width or $h < $height){ 
            return false;
        };

        $ratio = max($width/$w, $height/$h);
        $y= ($h - $height / $ratio) /2;
        $x = ($w - $width / $ratio) / 2;
        $h = $height / $ratio;
        $w = $width / $ratio;

    }
    else{
        if($w < $width and $h < $height) {
            return false;
        }

        $ratio = min($width/$w, $height/$h);
        $width = $w * $ratio;
        $height = $h * $ratio;
        $x = 0;
        $y = 0;
    }

    $new = imagecreatetruecolor($width, $height);

    // preserve transparency
    if($type == "gif" or $type == "png"){
        imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
        imagealphablending($new, false);
        imagesavealpha($new, true);
    }
    imagecopyresampled($new, $img, 0, 0, $x, $y, $width, $height, $w, $h);
    switch($type){
        case 'gif': imagegif($new, $dst); break;
        case 'jpg': imagejpeg($new, $dst); break;
        case 'png': imagepng($new, $dst); break;
    }
    return true;
}

// - - - formulaires


function deployer_formulaire_item($item=array(),$titre="", $submit="Go !",$traitement="ajouter_item",$addtop="",$addbottom=""){
    // entete du formulaire
    echo "<header>\n<h2>$titre</h2>\n</header>\n";
    echo $addtop;
    echo "<div class=\"form\">
        <form class=\"toplabel\" method=\"post\" enctype=\"multipart/form-data\">";

    // charger l'info s'il y a lieu
    if(!empty($item)){
        echo "<input type='hidden' name='url' value='".$item['url']."'>\n";
    } 

    deployer_champs($item, $GLOBALS['root'].$GLOBALS['definition_item']);
    echo $addbottom;
    echo "            <p>
                <input type=\"submit\" name=\"$traitement\" value=\"$submit\">
            </p>
        </form>";
}

function deployer_formulaire_selection($item=array(),$titre="", $submit="Go !",$traitement="ajouter_selection",$addtop="",$addbottom=""){
    // entete du formulaire
    echo "<header>\n<h2>$titre</h2>\n</header>\n";
    echo $addtop;
    echo "<div class=\"form\">
        <form class=\"toplabel\" method=\"post\" enctype=\"multipart/form-data\">";

    // charger l'info s'il y a lieu
    if(!empty($item)){
        echo "<input type='hidden' name='url' value='".$item['url']."'>\n";
    } 

    deployer_champs($item, $GLOBALS['root'].$GLOBALS['definition_selection']);
    echo $addbottom;
    echo "            <p>
                <input type=\"submit\" name=\"$traitement\" value=\"$submit\">
            </p>
        </form>";
}

function deployer_champs($item=array(),$definition){
    $xml = simplexml_load_file($definition);
    foreach($xml->children() as $champ) {

        $titre = $champ->attributes()->titre;
        $type = $champ->attributes()->type;

        echo "<fieldset class='".$type."'>\n<label for='".$champ->getName()."'>".$titre."</label>\n";

        if($type=="text"){
            echo "<input class='form-text' type='text' name='".$champ->getName()."' value='"._c($item,$champ->getName())."'>\n";
        }
        if($type=="textarea"){
            echo "<textarea name='".$champ->getName()."'>"._c($item,$champ->getName())."</textarea>\n";
        }
        /*
        if($type=="select"){
            echo "<select name='".$champ->getName()."'>\n";
            $t=_c($item,$champ->getName());
            foreach($champ->children() as $option) {
                echo "<option name='".$option."' ";
                if($t==$option){
                    echo " selected='selected'";
                }
                echo ">".$option."</option>\n";
            }
            echo "</select>\n";
        }
        */
        if($type=="image"){
            if(_c($item,$champ->getName())){
                deployer_form_image($champ->getName(),_c($item,$champ->getName()),$item['url']);

            } else {
                echo "<input class='upload-input' multiple='' name='".$champ->getName()."' type='file'>";
            }
        }

        if($type=="item"){
            // le code html de récepteur
            // la liste des items dedans avec le code de suppression

            echo "<div class=\"form-part open\" id=\"edit-items\">
            <header class=\"form-part-head\">
                <h3><a href=\"javascript:form_showhide('#edit-items');\"><i class=\"fa fa-chevron-right fa-fw fa-rotate-90\"></i> Items <span id=\"compteur\">(0)</span></h3></a>
            </header>
            <div class=\"form-part-content\">";


            if(isset($item['item'])){
                echo "<script>";
                if(is_array($item['item'])){
                    foreach($item['item'] as $item_url){
                        echo "manage_multi_selected_item('".$item_url."');";
                        //echo "$(\"#main-list li[data-url='".$item_url."']\").click();\n";
                    }
                } else {
                    echo "manage_multi_selected_item('".$item_url."');";
                    //echo "$(\"#main-list li[data-url='".$item['item']."']\").click();\n";
                }
                echo "</script>";
            }



            echo "</div>
        </div>";


        }

        if($type=="file"){
            $allow = $champ->attributes()->allow;
            // recuperer tous les éléments du xml qui correspondent
            if(isset($item["file"])){
                if(!is_array($item["file"])){
                    $item["file"]=array($item["file"]);
                }
                deployer_files($item['file'],$item['url'],1);
            }
            // afficher le form d'ajout
            echo "<input type='file' class='upload-input' multiple='' name='file[]' accept='".$allow."' >";
            echo "fichier de type ".$allow;
        }



        if($type=="date_creation"){
            echo "<input class='form-text' type='text' name='".$champ->getName()."' value='"._c($item,$champ->getName(),date("Y/m/d H:i:s"))."'>\n";
        }

        if($type=="date"){
            echo "<input class='form-text' type='text' name='".$champ->getName()."' value='"._c($item,$champ->getName())."'>\n";
        }

        echo "</fieldset>\n";
    }

}

/*  - - - - - - traitement des uploads des docs */
function traiter_upload_doc($upload){
    $retour=-1;

    if(is_array($upload['tmp_name'])){
        // plusieurs fichiers
        $retour=array();

        foreach($upload['name'] as $num => $nom){
            if(!empty($upload['name'][$num])){
                $single=array('name'=> $upload['name'][$num],
                              'type'=> $upload['type'][$num],
                              'tmp_name'=> $upload['tmp_name'][$num],
                              'error'=> $upload['error'][$num],
                              'size'=> $upload['size'][$num]
                             );

                $retour[]=traiter_upload_doc_unique($single);
            }
        }


    } else {
        // un seul fichier
        if(!empty($upload['name'])){
            $retour=array();
            $retour[]=traiter_upload_doc_unique($upload);
        }
    }

    return $retour;
}

function traiter_upload_doc_unique($upload){
    fix_exists_dir($GLOBALS['root'].$GLOBALS['dossier_images']);
    $tempFile = $upload['tmp_name'];
    // Validate the file type
    $fileTypes = explode(",",$GLOBALS['docs_allowed']);// par ex ('pdf','zip', 'jpg', 'png', 'gif'); // File extensions
    $fileParts = pathinfo($upload['name']);

    if (in_array($fileParts['extension'],$fileTypes)) {
        $ext = pathinfo($upload['name'], PATHINFO_EXTENSION);  //figures out the extension 
        $nom_seul=stripAccents($fileParts['filename']);

        // ici tester le nom du fichier pour permettre les doublons
        if(file_exists($GLOBALS['root'].$GLOBALS['dossier_images'].$nom_seul.".".$ext)){
            $ad= hash('adler32', date("Y-m-d H:i:s"));
            $nom_seul=$nom_seul."_".$ad; // ce serait mieux entre le nom et l'extension
        }

        $retour=$GLOBALS['dossier_images'].$nom_seul.".".$ext;
        move_uploaded_file($tempFile, $GLOBALS['root'].$GLOBALS['dossier_images'].$nom_seul.".".$ext);

        return $retour;

    } else {
        return -1;
    }


}

function traiter_rotation_image($filename, $degrees){
    $new_file=$filename; // au même endroit
    $rotang = $degrees;

    $fileParts = pathinfo($filename);

    $versions=glob($GLOBALS['root'].$GLOBALS['dossier_vignettes'].$fileParts['basename']."*");
    foreach ($versions as $version){
        unlink($version); 
    }
    // virer les versions de cette images dans les vignettes !
    // sinon le travail sera invisible...

    list($width, $height, $type, $attr) = getimagesize($filename);
    $size = getimagesize($filename);


    switch($size['mime']){
        case 'image/jpeg':
            $source =imagecreatefromjpeg($filename);
            $bgColor=imageColorAllocateAlpha($source, 0, 0,0, 0);
            $rotation = imagerotate($source,$rotang,$bgColor);
            imagealphablending($rotation, false);
            imagesavealpha($rotation, true);
            imagecreate($width,$height);
            imagejpeg($rotation,$new_file);
            chmod($filename, 0777);
            break;
        case 'image/png':

            $source =imagecreatefrompng($filename);
            $bgColor=imageColorAllocateAlpha($source, 0, 0,0, 0);
            $rotation = imagerotate($source,$rotang,$bgColor);
            imagealphablending($rotation, false);
            imagesavealpha($rotation, true);
            imagecreate($width,$height);
            imagepng($rotation,$new_file);
            chmod($filename, 0777);
            break;
        case 'image/gif':

            $source =imagecreatefromgif($filename);
            $bgColor=imageColorAllocateAlpha($source, 0, 0,0, 0);
            $rotation = imagerotate($source,$rotang,$bgColor);
            imagealphablending($rotation, false);
            imagesavealpha($rotation, true);
            imagecreate($width,$height);
            imagegif($rotation,$new_file);
            chmod($filename, 0777);
            break;
        case 'image/vnd.wap.wbmp':
            $source =imagecreatefromwbmp($filename);
            $bgColor=imageColorAllocateAlpha($source, 0, 0,0, 0);
            $rotation = imagerotate($source,$rotang,$bgColor);
            imagealphablending($rotation, false);
            imagesavealpha($rotation, true);
            imagecreate($width,$height);
            imagewbmp($rotation,$new_file);
            chmod($filename, 0777);
            break;
    }

}

function deployer_files($files,$item_url,$admin=0){
    $infos=array();
    $ext_image=array('jpg','png','gif');
    $ext_video=array('avi','mp4','mkv','m4v');
    $ext_autre=array('zip','txt','rtf','pdf','doc','msi','rar','dmg','js');
    $ext_son=array('mp3','aac','ogg');

    foreach($files as $file){
        $ext=strtolower(substr(strrchr($file,"."),1));
        if(in_array($ext,$ext_image)){
            $infos['Images'][]=$file;
        }
        if(in_array($ext,$ext_son)){
            $infos['Sons'][]=$file;
        }
        if(in_array($ext,$ext_autre)){
            $infos['Download'][]=$file;
        }
        if(in_array($ext,$ext_video)){
            $infos['Vidéos'][]=$file;
        }
    }

    echo "<ul class='file-list'>";
    // afficher ces trucs
    if (array_key_exists('Images', $infos)) {
        foreach($infos['Images'] as $image){
            echo "<li><a href='".$image."' class='fancybox file-icone' rel='same'><img src='".donne_lien($image,$l_vignette=60,$h_vignette=60,true)."' alt='image'></a>";
            echo "<span>".fichier_seul($image)."</span>";
            if($admin!=0){
                echo "<a href='javascript:doc_delete(\"".$image."\",\"".$item_url."\");' class='file-delete'><i class='fa fa-times'></i></a>";
            }
            echo "<input type='hidden' name='file[]' value='".$image."'>";
            echo"</li>";
        }
    }

    // autres
    $liste_type=array('Sons','Vidéos','Download');

    foreach($liste_type as $type){
        if (array_key_exists($type, $infos)) {
            foreach($infos[$type] as $lien){
                echo "<li><a href='".$lien."' class='file-icone'>".$GLOBALS['icone'][$type]."</a>";
                echo "<span>".fichier_seul($lien)."</span>";

                if($admin!=0){
                    echo "<a href='javascript:doc_delete(\"".$lien."\",\"".$item_url."\");' class='file-delete'><i class='fa fa-times'></i></a>";
                }
                echo "<input type='hidden' name='file[]' value='".$lien."'>";

                echo "</li>";
            }

        }
    }
    echo "</ul>";

}

// - - - timer 

class loadTime {
    var $timestart;
    var $digits;

    public function __construct($digits = ""){
        $this->timestart    = explode (' ', microtime());
        $this->digits       = $digits;
    }

    public function finit(){
        $timefinish         = explode (' ', microtime());
        if($this->digits == ""){
            $runtime_float  = $timefinish[0] - $this->timestart[0];
        }else{
            $runtime_float  = round(($timefinish[0] - $this->timestart[0]), $this->digits);
        }
        $runtime = ($timefinish[1] - $this->timestart[1]) + $runtime_float;
        echo "Page exécutée en $runtime secondes\n";
    }
} 

?>