<?php
include("custom/config.php");
include($GLOBALS['root']."includes/inc-functions.php");
include_once($GLOBALS['root'].'includes/inc-headers-process.php');
include_once($GLOBALS['root'].'includes/inc-header.php');
?>
<div id="main-display" class="colonne full clearfix">
    <section class="content-display" id="mosaic">
        <div id="waiter" class="waiting"><i class="fa fa-circle-o-notch fa-spin"></i></div>
        <ul id="main-list" class="clearfix"></ul>
    </section>
</div>
<script>
    $(document).ready(function(){

        $.ajax({
            url: "index-load-collection.php",
            cache: false
        }).done(function( html ) {
            $("#waiter").fadeOut("fast");
            $( "#main-list" ).append( html );
            search_init();
        });

    });
</script>
<?php
include_once($GLOBALS['root'].'includes/inc-footer.php');
?>
