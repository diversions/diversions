<?php
include("../custom/config.php"); // fichier de config
include("../includes/inc-functions.php");
$donnees=array();
$ecrase='false';
$titre='Ajouter une collection';
$back='ajax/ajax-load-selections.php';
if(isset($_GET['selection'])){
    $donnees=read_xml($_GET['selection']);
    $ecrase='true';
    $titre='Modifier une collection';
    $back='ajax/ajax-load-selection.php?selection='.$_GET['selection'];
}
?>
<div id="main-editor-tools" class="clearfix">
    <div class='editor-tools-left clearfix'> 
        <a id='editor-back' class='tool-button' title='Back' href='#' onclick='dispatch_interface(this)' data-class='selection' data-load='<?php echo $back; ?>' data-clic-action='one' data-cas='form'><i class='fa fa-chevron-left'></i></a>
    </div>

    <div class='editor-tools-right'><a class='tool-button small-carefull' title='delete' href='javascript:cache_colonne_display();'>Cancel <i class='fa fa-chevron-right fa-fw'></i></a>
    </div>
</div>
<article>
    <?php
    deployer_formulaire_selection($donnees,$titre,"Go");
    ?></article>

<script>
    //$("#main-list .item").removeClass("selected");
    $('.form form').on('submit', function(e) {

        var $this = $(this); // L'objet jQuery du formulaire
        $this.append("<input type='hidden' name='ajouter_selection' value='oui'>");
        $this.append("<input type='hidden' name='ecrase' value='<?php echo $ecrase; ?>'>");
        e.preventDefault(); // J'empêche le comportement par défaut du navigateur, c-à-d de soumettre le formulaire

        var formData = new FormData($(this)[0]);

        // Envoi de la requête HTTP en mode asynchrone
        $.ajax({
            url: "ajax/ajax-forms.php", // Le nom du fichier indiqué dans le formulaire
            type: "post", // La méthode indiquée dans le formulaire (get ou post)
            data: formData, //$this.serialize(), // Je sérialise les données (j'envoie toutes les valeurs présentes dans le formulaire)
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function(html) { // Je récupère la réponse du fichier PHP : l'adresse du fichier
            $("#editor-back").click();
        }
               });
    });
</script>