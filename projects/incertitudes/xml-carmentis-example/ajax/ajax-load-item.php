<?php
include("../custom/config.php"); // fichier de config
include("../includes/inc-functions.php");
if(isset($_POST['item'])){
    $file=$_POST['item'];
} else {
    echo "Pas d'item";
    die();   
}

$item=read_xml($file);
?>
<div id="main-editor-tools" class="clearfix">
    <?php     
    echo "<div class='editor-tools-left clearfix'>";    
    echo "<a id='visualise-info' class='tool-button' title='".htmlentities($item['title'])."' href='".$item['illustration']."' onclick='return call_f(this);'><i class='fa fa-eye'></i></a>";
    echo "<a class='tool-button'  title='previous' href='javascript:vignette_precedente()'><i class='fa fa-chevron-left'></i></a>";
    echo "<a class='tool-button'  title='next' href='javascript:vignette_suivante()'><i class='fa fa-chevron-right'></i></a>";

    if(isset($_SESSION['nom'])){
        echo "<a class='tool-button carefull' title='delete' href='javascript:item_delete(\"".$item['url']."\");'><i class='fa fa-times'></i></a>";
        echo "<a class='tool-button' title='edit' href='javascript:item_update_info(\"".$item['url']."\");'><i class='fa fa-pencil'></i></a>";
    }
    echo "</div>";
    ?>
    <div class='editor-tools-right'>
        <a class='tool-button small-carefull' title='delete' href='javascript:cache_colonne_display();'>Close <i class='fa fa-chevron-right fa-fw'></i></a>
    </div>
</div>
<article>
    <?php
    echo deployer_info($item); 
    ?>


</article>
<script>
    update_f();
</script>