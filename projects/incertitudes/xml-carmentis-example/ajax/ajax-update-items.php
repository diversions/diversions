<?php
include("../custom/config.php"); // fichier de config
include("../includes/inc-functions.php");

?>
<div id="main-editor-tools" class="clearfix">
    <div class='editor-tools-right'><a class='tool-button small-carefull' title='delete' href='javascript:cache_colonne_display();deselect_all_items();'>Cancel <i class='fa fa-chevron-right fa-fw'></i></a>
    </div>
</div>
<article>

    <header class="editor-title"><h2>Modifications multiples</h2>
        <div class="cadre-info">Sélectionnez les éléments à modifier dans la liste principale en les cliquant. Choisissez ensuite une action ci-dessous.</div>
    </header>
    <form id="form-update" class="toplabel form" method="post" enctype="multipart/form-data">

        <div class="form-part open" id="edit-items">
            <header class="form-part-head">
                <h3><a href="javascript:form_showhide('#edit-items');"><i class="fa fa-chevron-right fa-fw fa-rotate-90"></i> Items <span id="compteur">(0)</span></h3></a>
            </header>
            <div class="form-part-content">

            </div>
        </div>

        <div class="form-part close" id="delete-item">
            <header class="form-part-head">
                <h3><a href="javascript:form_showhide('#delete-item');"><i class="fa fa-chevron-right fa-fw"></i> Suppression</h3></a>
            </header>
            <div class="form-part-content">
                <a href="javascript:submitwith('delete_all','1')" class="tool-button">Supprimer les éléments</a>
            </div>
        </div>

        <div class="form-part close" id="rotate-item">
            <header class="form-part-head">
                <h3><a href="javascript:form_showhide('#rotate-item');"><i class="fa fa-chevron-right fa-fw"></i> Rotation</h3></a>
            </header>
            <div class="form-part-content">
                <a href="javascript:submitwith('rotate_all','90')" class="tool-button"><i class="fa fa-undo"></i></a>
                <a href="javascript:submitwith('rotate_all','-90')" class="tool-button"><i class="fa fa-repeat"></i></a>
            </div>
        </div>

        <div class="form-part close" id="edit-form">
            <header class="form-part-head">
                <h3><a href="javascript:form_showhide('#edit-form');"><i class="fa fa-chevron-right fa-fw"></i> Champs</h3></a>
            </header>
            <div class="form-part-content">
                <?php deployer_champs($item=array(), $GLOBALS['root'].$GLOBALS['definition_item']); ?>
                <a href="javascript:submitwith('update_all','oui')" class="tool-button">Mettre à jour</a>
            </div>

        </div>


    </form>

</article>
<script>
    function submitwith(n,v){
        $("#form-update").append("<input id='type-action' type='hidden' name='"+n+"' value='"+v+"'>");
        $('#form-update').submit();
    }

    $("#main-list .item").removeClass("selected");

    $('#form-update').on('submit', function(e) {

        e.preventDefault(); // J'empêche le comportement par défaut du navigateur, c-à-d de soumettre le formulaire
        var $this = $(this); // L'objet jQuery du formulaire
        var formData = new FormData($(this)[0]);

        $.ajax({
            url: "ajax/ajax-forms.php", // Le nom du fichier indiqué dans le formulaire
            type: "post", // La méthode indiquée dans le formulaire (get ou post)
            data: formData, //$this.serialize(), // Je sérialise les données (j'envoie toutes les valeurs présentes dans le formulaire)
            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function(html) { // Je récupère la réponse du fichier PHP
            // en fonction de l'action, un refresh différent 
            var type_action=$("#form-update #type-action").attr("name");
            switch(type_action) {
            case "update_all":
            case "rotate_all":
            // updater la vignette
            $("#form-update #edit-items input").each(function(){
            var item_url= $(this).attr("value");
            item_vignette_updater(item_url);
        });
        setTimeout(function(){ refresh_icons_managed();}, 1000);
        break;
        case "delete_all":
        $("#form-update #edit-items input").each(function(){
            var item_url= $(this).attr("value");
            $("#main-list li[data-url='"+item_url+"']").remove();
        });
        $("#form-update #edit-items .form-part-content").empty();
        compte_items_managed();
        break;
        default:
        // rien par defaut
    }
                         quicksearch.cache();
    }
    });

    });
</script>