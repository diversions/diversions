<?php
include("../custom/config.php"); // fichier de config
include("../includes/inc-functions.php");
?>
<div id="main-editor-tools" class="clearfix">
     <div class='editor-tools-left'>
    <?php
    if(isset($_SESSION['nom'])){
        
        echo "<a class='tool-button' title='Ajouter sélection' onclick='dispatch_interface(this);' href='#' data-load='ajax/ajax-add-selection?item=".$item['url']."' data-class='selection' data-clic-action='multi' data-cas='form'><i class='fa fa-plus'></i></a>";
    }
    ?>
    </div>
    
    <div class='editor-tools-right'><a class='tool-button small-carefull' title='delete' href='javascript:cache_colonne_display();'>Cancel <i class='fa fa-chevron-right fa-fw'></i></a>
    </div>
</div>
<h2>Liste des sélections</h2>
<ul>
    <?php
    $liste = glob($GLOBALS['root'].$GLOBALS['dossier_selections']."*.xml", GLOB_BRACE);
    if(is_array($liste)){
        foreach($liste as $url){ 
            $url=str_replace($GLOBALS['root'],"",$url);
            $item = read_xml($url);

            echo "<li><a href='#' onclick='dispatch_interface(this)' data-load='ajax/ajax-load-selection.php?selection=$url' data-clic-action='one' data-cas='form' data-class='selection'>".$item['title']."</a>";
            echo " (".count($item['item'])." items )";
            echo "</li>";
        }
    }
    ?>
</ul>