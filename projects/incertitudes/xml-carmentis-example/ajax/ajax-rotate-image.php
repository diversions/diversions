<?php //image rotate code here 
         if(isset($_POST['url'])){
             ini_set('memory_limit', '256M');
             include("../custom/config.php"); // fichier de config
             
             $filename = $GLOBALS['root'].$_POST['url'];
             $degrees=$_POST['angle'];
             
             $new_file=$filename; // au même endroit
             $rotang = $degrees;
             
             $fileParts = pathinfo($_POST['url']);
             
             $versions=glob($GLOBALS['root'].$GLOBALS['dossier_vignettes'].$fileParts['basename']."*");
             foreach ($versions as $version){
              unlink($version);   
             }
             
             // virer les versions de cette images dans les vignettes !
             // sinon le travail sera invisible...
             
             list($width, $height, $type, $attr) = getimagesize($filename);
              $size = getimagesize($filename);

              switch($size['mime']){
                 case 'image/jpeg':
                                     $source =imagecreatefromjpeg($filename);
                                     $bgColor=imageColorAllocateAlpha($source, 0, 0,0, 0);
                                     $rotation = imagerotate($source,$rotang,$bgColor);
                                     imagealphablending($rotation, false);
                                     imagesavealpha($rotation, true);
                                     imagecreate($width,$height);
                                     imagejpeg($rotation,$new_file);
                                     chmod($filename, 0777);
                 break;
                 case 'image/png':

                                     $source =imagecreatefrompng($filename);
                                     $bgColor=imageColorAllocateAlpha($source, 0, 0,0, 0);
                                     $rotation = imagerotate($source,$rotang,$bgColor);
                                     imagealphablending($rotation, false);
                                     imagesavealpha($rotation, true);
                                     imagecreate($width,$height);
                                     imagepng($rotation,$new_file);
                                     chmod($filename, 0777);
                 break;
                 case 'image/gif':

                                     $source =imagecreatefromgif($filename);
                                     $bgColor=imageColorAllocateAlpha($source, 0, 0,0, 0);
                                     $rotation = imagerotate($source,$rotang,$bgColor);
                                     imagealphablending($rotation, false);
                                     imagesavealpha($rotation, true);
                                     imagecreate($width,$height);
                                     imagegif($rotation,$new_file);
                                     chmod($filename, 0777);
                 break;
                 case 'image/vnd.wap.wbmp':
                                     $source =imagecreatefromwbmp($filename);
                                     $bgColor=imageColorAllocateAlpha($source, 0, 0,0, 0);
                                     $rotation = imagerotate($source,$rotang,$bgColor);
                                     imagealphablending($rotation, false);
                                     imagesavealpha($rotation, true);
                                     imagecreate($width,$height);
                                     imagewbmp($rotation,$new_file);
                                     chmod($filename, 0777);
                 break;
              }
         }
    ?>