<?php
include("../custom/config.php"); // fichier de config
include("../includes/inc-functions.php");

if(isset($_POST['item'])){
    $file=$_POST['item'];
} else {
    echo "Pas d'item";
    die();   
}
?>
<div id="main-editor-tools" class="clearfix">
    <div class="editor-tools-right"><a class='tool-button small-carefull' title='delete' href='javascript:cache_colonne_display();'>Cancel <i class='fa fa-chevron-right fa-fw'></i></a></div>
</div>
<article>
    <?php
    $item=read_xml($file);
    deployer_formulaire_item($item,"Updater un item", "Updater","updater_item");
    ?>
</article>

<script>
    $('.form form').on('submit', function(e) {
        e.preventDefault(); // J'empêche le comportement par défaut du navigateur, c-à-d de soumettre le formulaire
        var item_url=$(this).find("input[name='url']").val();
        var $this = $(this); // L'objet jQuery du formulaire
        $this.append("<input type='hidden' name='update_item' value='oui'>");

        var formData = new FormData($(this)[0]);
        // Envoi de la requête HTTP en mode asynchrone
        $.ajax({

            url: "ajax/ajax-forms.php", // Le nom du fichier indiqué dans le formulaire
            type: "post", // La méthode indiquée dans le formulaire (get ou post)
            data: formData, //$this.serialize(), // Je sérialise les données (j'envoie toutes les valeurs présentes dans le formulaire)
            async: false,
            cache: false,
            contentType: false,
            processData: false,

            success: function(html) { // Je récupère la réponse du fichier PHP
            // plus, updater la petite vignette
            item_vignette_updater(item_url);
            $("#main-list li[data-url='"+item_url+"']").click();
            quicksearch.cache();
        }
               });
    });
</script>