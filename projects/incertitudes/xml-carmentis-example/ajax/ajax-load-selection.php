<?php
include("../custom/config.php"); // fichier de config
include("../includes/inc-functions.php");

$url=false;
if(isset($_GET['selection'])){
    $url=$_GET['selection'];
} else {
    echo "nothing answered !";
    die();
}

?>
<div id="main-editor-tools" class="clearfix">
    <div class='editor-tools-left'>
     <a id='editor-back' class='tool-button' title='Back' href='#' onclick='dispatch_interface(this)' data-class='selection' data-load='ajax/ajax-load-selections.php' data-clic-action='one' data-cas='form'><i class='fa fa-chevron-left'></i></a><?php
    if(isset($_SESSION['nom'])){
        echo "<a class='tool-button carefull' title='delete' href='javascript:selection_delete(\"".$url."\");'><i class='fa fa-times'></i></a>";

  echo "<a class='tool-button' title='Editer sélection' onclick='dispatch_interface(this);' href='#' data-load='ajax/ajax-add-selection?selection=".$url."' data-class='selection' data-clic-action='multi' data-cas='form'><i class='fa fa-pencil'></i></a>";

    }

    ?>
    </div>
    <div class='editor-tools-right'><a class='tool-button small-carefull' title='delete' href='javascript:cache_colonne_display();'>Cancel <i class='fa fa-chevron-right fa-fw'></i></a>
    </div>
</div>

<?php

if($url){
    $selection = read_xml($url);
    echo "<article id='selection'>";
    echo "<h2>".$selection['title']."</h2>";
    echo "<div class='texte mini'>".$selection['author']."</div>";
    echo "<div class='texte'>".$selection['note']."</div>";
    echo "</article>";

} 
?>
<script>
    var t=$("#selection h2").html();
    $("#display-statut").append("<span id='display-statut-selection'><a href='#' onclick='dispatch_interface(this)' data-load='ajax/ajax-load-selection.php?selection=<?php echo $url; ?>' data-clic-action='one' data-cas='form' data-class='selection' class='tool-button'>Sélection  &#34;"+t+"&#34;</a><a href='javascript:remove_selection();' class='tool-button'><i class='fa fa-times small-carefull'></i></a></span>");

    function display_selection_items(){
        $("#main-list li").removeClass(".selected");

        var items = [<?php
            foreach($selection['item'] as $item){ echo "\"".$item."\",";}
            ?>];
        items.forEach(function(item) {
            $("#main-list li[data-url='"+item+"']").addClass("selection-selected");
        });
        $("#main-list li").not(".selection-selected").fadeOut();
    }
    display_selection_items();
</script>