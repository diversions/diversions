<?php
    include("../custom/config.php"); // fichier de config
    include("../includes/inc-functions.php");
?>
<div id="main-editor-tools" class="clearfix">
    <div class='editor-tools-right'><a class='tool-button small-carefull' title='delete' href='javascript:cache_colonne_display();'>Cancel <i class='fa fa-chevron-right fa-fw'></i></a>
    </div>
</div>
<article>
   <?php
    deployer_formulaire_item(array(),"Ajouter un item","Ajouter");
    ?></article>
<script>
    $('.form form').on('submit', function(e) {

        var $this = $(this); // L'objet jQuery du formulaire
        $this.append("<input type='hidden' name='ajouter_item' value='oui'>");
        e.preventDefault(); // J'empêche le comportement par défaut du navigateur, c-à-d de soumettre le formulaire

        var formData = new FormData($(this)[0]);

        // Envoi de la requête HTTP en mode asynchrone
        $.ajax({
            url: "ajax/ajax-forms.php", // Le nom du fichier indiqué dans le formulaire
            type: "post", // La méthode indiquée dans le formulaire (get ou post)
            data: formData, //$this.serialize(), // Je sérialise les données (j'envoie toutes les valeurs présentes dans le formulaire)
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function(html) { // Je récupère la réponse du fichier PHP : l'adresse du fichier
            item_vignette_ajouter(html);
            cache_colonne_display();
            quicksearch.cache();
            //$("#bouton-fullwidth").click();
        }
               });
    });
</script>