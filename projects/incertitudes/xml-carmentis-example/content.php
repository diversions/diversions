<div id="main-editor-tools" class="clearfix">
    <div class='editor-tools-right'><a class='tool-button small-carefull' title='delete' href='javascript:cache_colonne_display();'>Close <i class='fa fa-chevron-right fa-fw'></i></a></div>
</div>
<article>
    <?php
    include("custom/config.php");
    include($GLOBALS['root']."includes/inc-functions.php");
    if(isset($_GET['page'])){
        $contenu=contenu_xml($_GET['page']);
    ?>

    <?php
        echo "<h2>".$contenu['titre']."</h2>\n<div class='texte'>";
        echo $contenu['texte'];
        echo "</div>\n";
    } else {
        echo "No Content !";
    }
    ?>
</article>