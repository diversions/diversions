// controle que les champs associés à la classe obligatoire soient remplis
// controle de la structure et de l'écriture des mails
var quicksearch;
$(document).ready(function(){
    $(".display-liste").click(function(){
        $("#display-tools a").removeClass("on");
        $(this).addClass("on");
        var c=$(this).attr("data-id");
        $(".content-display").attr("id",c);
        return false;
    });

});


function reorder(sens){
    var sortByName = $.sortFunc(["$(this).find('.titre').text()"]);
    var sortById = $.sortFunc(["$(this).attr('data-url')"]);
    
    if(sens == 'up'){
        $("#main-list").sortChildren(sortByName, 0, 0, true);
    } 
    if(sens =='down') {
        $("#main-list").sortChildren(sortByName, 0, 0, false);

    }
    if(sens =='id') {
        $("#main-list").sortChildren(sortById, 0, 0, false);

    }
    //$(this).toggleClass("reverse")
}


/* gestion du clavier */

$(document).keydown(function(e){
    var statut=$("#main-editor").attr("data-clic-action");
    if(statut=="one"){
        if (e.keyCode == 39){
            vignette_suivante();
        } else if (e.keyCode == 37){ 
            vignette_precedente();
        }

    }
});

function sidebar_action(item_url){
    var statut=$("#main-editor").attr("data-clic-action");
    switch(statut) {
        case "one":
            item_show_info(item_url);
            break;
        case "multi":
            manage_multi_selected_item(item_url);
            //alert("multi");
            break;
        default:
            item_show_info(item_url);
    } 
}

function vignette_suivante(){
    var i=$("#main-list li.selected").eq(0);
    var n=$(i).next("li");
    if(n.length>0){
        $(n).addClass("selected").click();
        $(i).removeClass("selected");
    }
}

function vignette_precedente(){
    var i=$("#main-list li.selected").eq(0);
    var p=$(i).prev("li");
    if(p.length>0){
        $(i).removeClass("selected");
        $(p).addClass("selected").click();
    }
}

function update_f(){
    if($(".fancybox-overlay").length){
        $("#main-editor #visualise-info").click();
    }
}

function manage_multi_selected_item(item_url){
    var elem =$("#main-list li[data-url='"+item_url+"']");
    $(elem).toggleClass("selected");
    if($(elem).hasClass("selected")){
        // ajouter l'élément
        var image=$(elem).find(".illustration").attr("src");
        var titre=$(elem).find(".titre").html();
        $("#edit-items .form-part-content").append("<div class='cartouche-item texte-mini' data-item-url='"+item_url+"'><img src='"+image+"'><span >"+titre+"</span><a href='javascript:retire_item_managed(\""+item_url+"\")'><i class='fa fa-times'></i></a><input type='hidden' name='item[]' value='"+item_url+"'></div>");
        compte_items_managed();

    } else {
        retire_item_managed(item_url);
    }
}

function retire_item_managed(item_url){
    $("#edit-items .form-part-content .cartouche-item[data-item-url='"+item_url+"'").remove();
    $("#main-list li[data-url='"+item_url+"']").removeClass("selected");
    compte_items_managed();
}

function compte_items_managed(){
    var nb=$("#main-list li.selected").length;
    $("#edit-items #compteur").html("("+nb+")");
}

function refresh_icons_managed(){
    $("#edit-items .form-part-content .cartouche-item").each(function(){
        var item_url=$(this).attr("data-item-url");
        var img_src=$("#main-list li[data-url='"+item_url+"'] .illustration").attr("src");
        var title=$("#main-list li[data-url='"+item_url+"'] span").html();
        $("#edit-items .cartouche-item[data-item-url='"+item_url+"'] img").attr("src",img_src);
        $("#edit-items .cartouche-item[data-item-url='"+item_url+"'] span").html(title);
    });
}
/*  */

function form_showhide(elem){
    $(elem).toggleClass("close").toggleClass("open");
    $(elem+ " header .fa").toggleClass("fa-rotate-90");
}

function init_login(){
    $(".form-login .login").addClass("login-off").click(function(){
        $(".form-login").removeClass("login-off").addClass("login-on");
    });
    $('.form-login').on('submit', function(e) {
        e.preventDefault(); // J'empêche le comportement par défaut du navigateur, c-à-d de soumettre le formulaire
        var $this = $(this); // L'objet jQuery du formulaire

        // Envoi de la requête HTTP en mode asynchrone
        $.ajax({
            url: "ajax/ajax-forms.php", // Le nom du fichier indiqué dans le formulaire
            type: "post", // La méthode indiquée dans le formulaire (get ou post)
            data: $this.serialize(), // Je sérialise les données (j'envoie toutes les valeurs présentes dans le formulaire)
            success: function(html) { // Je récupère la réponse du fichier PHP
                if(html==""){
                    $(".adminnav").load("menus/admin-menu.php");
                } else {
                    alert(html);
                }
            }
        });
    });
}

/* grosse fonction de déploiement de l'interface à partir de clics
gestion du #main-display, du #main-editor, du #display-statut, des sélections

->
retirer la classe "selected" sur les éléments
retirer la classe "selected-collection" sur les éléments 
ajouter la classe correspondante sur #main-display
ajouter la classe "one" ou "multi" sur #main-editor
charger et déployer une adresse dans le #main-editor


*/
function dispatch_interface(ceci){
    var cas=$(ceci).attr("data-cas");

    if(cas =="close"){
        // simple fermeture
        // en fonction du cas, deselectionner, etx.
        cache_colonne_display();

    } else {
        // chargement avec option
        // charger l'url et dans le done, executer le switch
        var lurl=$(ceci).attr("data-load");
        var cl=$(ceci).attr("data-class");
        var act=$(ceci).attr("data-clic-action");

        $("#main-list").attr("class",cl);
        deselect_all_items();

        $.ajax({
            method: "POST",
            url: lurl,
            //data: { name: "John", location: "Boston" },
            beforeSend: function( x ) {
                // avant le chargement
                switch(cas) {
                    case 'logout':
                        // pas d'ouverture
                        break;

                    default:
                        montre_colonne_display();
                        $("#main-editor").empty().addClass("wait");
                } 

            }
        }).done(function( msg ) {
            // callbacks
            switch(cas) {
                case 'page':
                    $("#main-editor").empty().append(msg).removeClass("wait").attr("data-clic-action",act);
                    break;
                case 'form':
                    $("#main-editor").empty().append(msg).removeClass("wait").attr("data-clic-action",act);
                    break;
                case 'logout':
                    $('.adminnav').load('menus/form-login.php');
                    cache_colonne_display();
                    break;

                default:
                    alert('defaut');
                    // retirer la class de #main-list
                    // charger l'url dans le #main-editor avec tout le protocole standard (wait, etc.)
                    // ne rien faire pour #display-statut
            } 
        });

    }
    return false;
}

function init_admin_nav(){
    $(".ajax-load").click(function(){
        dispatch_interface(this);
        return false;
        /*
        var lurl=$(this).attr("data-load");
        var cl=$(this).attr("data-class");
        var act=$(this).attr("data-clic-action");
        $("#main-list").attr("class",cl);
        deselect_all_items();
        $.ajax({
            method: "POST",
            url: lurl,
            //data: { name: "John", location: "Boston" },
            beforeSend: function( x ) {
                montre_colonne_display();
                $("#main-editor").empty().addClass("wait");
            }
        })
            .done(function( msg ) {
            $("#main-editor").empty().append(msg).removeClass("wait").attr("data-clic-action",act);
        });

        return false;
        */
    }); 

}

function remove_selection(){
    $("#display-statut-selection").fadeOut("slow", function() {
        $("#display-statut-selection").remove();

    });
    cache_colonne_display();
    deselect_all_items();
}

function deselect_all_items(){
    $("#main-list li").removeClass("selected").removeClass("selection-selected").fadeIn();
}

function call_f(elem) {
    elem = $(elem);
    if (!elem.data("fancybox")) {
        elem.data("fancybox", true);
        elem.fancybox({
            openEffect : 'none',
            closeEffect: 'none',
            openSpeed: 0,
            closeSpeed: 0
        });
        elem.fancybox().trigger('click');
    }
    return false; 
}

function search_init(){
    $(".searchlink").show();
    quicksearch = $('input.small-input').quicksearch('#main-list li',{
        'delay':100,
        'minValLength': 2,
    });   
}

function show_search(){
    $(".small-input").toggle();
    $(".small-input").focus();
}

/* gstion des clics et de la sidebar - - - - - - - - -  */

function montre_colonne_display(){
    $("body").removeClass("fullwidth").addClass("right-reduced");
}

function cache_colonne_display(){
    $("body").removeClass("right-reduced").addClass("fullwidth");
    $("#main-editor").attr("data-clic-action","open");
    deselect_all_items();
}

function item_show_info(item_url){
    $.ajax({
        method: "POST",
        url: "ajax/ajax-load-item.php",
        data: { item: item_url },
        beforeSend: function( x ) {
            $("#main-list .item").removeClass("selected");
            $("li[data-url='"+item_url+"']").addClass("selected");
            $("#main-editor").empty().addClass("wait");
            montre_colonne_display();
        }
    }).done(function( msg ) {
        $("#main-editor").empty().append(msg).removeClass("wait");
        $("#main-editor").attr("data-clic-action","one");
    });
}

function item_update_info(item_url){
    $.ajax({
        method: "POST",
        url: "ajax/ajax-load-update-item.php",
        data: { item: item_url },
        beforeSend: function( x ) {
            $("#tools-display").empty().addClass("wait");
            montre_colonne_display();

        }
    }).done(function( msg ) {
        $("#main-editor").empty().append(msg).removeClass("wait");
        $("#main-editor").attr("data-clic-action","one");
    });
}
function item_delete(item_url){
    $.ajax({
        method: "POST",
        url: "ajax/ajax-forms.php",
        data: { item: item_url, supprimer_item: 1 },
        beforeSend: function( x ) {
            $("#tools-display").empty().addClass("wait");

        }
    }).done(function( msg ) {
        $("#main-editor").empty().append(msg).removeClass("wait");
        cache_colonne_display();
        $("li[data-url='"+item_url+"']").remove();
        quicksearch.cache();
    });
}

function selection_delete(selection_url){
    $.ajax({
        method: "POST",
        url: "ajax/ajax-forms.php",
        data: { selection: selection_url, supprimer_selection: 1 },
        beforeSend: function( x ) {
            $("#tools-display").empty().addClass("wait");

        }
    }).done(function( msg ) {
        alert(msg);
        //$("#main-editor").empty().append(msg).removeClass("wait");
        cache_colonne_display();
        //$("li[data-url='"+item_url+"']").remove();
        //quicksearch.cache();
    });
}

function item_vignette_updater(item_url){
    $.ajax({
        method: "POST",
        url: "ajax/ajax-forms.php",
        data: { url: item_url, genere_vignette: 1 }
    }).done(function( msg ) {
        $("li[data-url='"+item_url+"']").replaceWith(msg);
        var ou=$("li[data-url='"+item_url+"']").offset().top;
        $('html,body').animate({scrollTop: ou-138}, 500);

    });
}

function item_vignette_ajouter(item_url){
    $.ajax({
        method: "POST",
        url: "ajax/ajax-forms.php",
        data: { url: item_url, genere_vignette: 1 }
    }).done(function( msg ) {
        $("#main-list").append(msg);
        var ou=$("li[data-url='"+item_url+"']").offset().top;
        $('html,body').animate({scrollTop: ou-138}, 500);
    });
}

function image_rotate(image_url,angle,item_url){
    $.ajax({
        method: "POST",
        url: "ajax/ajax-forms.php",
        data: { url: image_url, angle: angle, rotation_image: 1 },
        beforeSend: function( x ) {
            $("#main-editor").empty().addClass("wait");
            montre_colonne_display();
        }
    }).done(function( msg ) {
        item_update_info(item_url);
        item_vignette_updater(item_url);
    });
}

function image_delete(image_url,item_url){
    $.ajax({
        method: "POST",
        url: "ajax-delete-image.php",
        data: { url: image_url, item: item_url },
        beforeSend: function( x ) {
            //$("#tools-display").empty().addClass("wait");
            //montre_colonne_display();
        }
    }).done(function( msg ) {
        item_update_info(item_url);
        // cache le hidden, montre le input
    });
}

function doc_delete(doc_url,item_url){
    $.ajax({
        method: "POST",
        url: "ajax-delete-document.php",
        data: { url: doc_url, item: item_url },
        beforeSend: function( x ) {
            //$("#tools-display").empty().addClass("wait");
            //montre_colonne_display();
        }
    }).done(function( msg ) {
        item_update_info(item_url);
        // cache le hidden, montre le input
    });

}
