#!/usr/bin/env/ python
# encoding=utf8  

# using scrape.py by Michael Murtaugh, DiVersions, Brussels, 2016: 
# scrape.py generates a json file that is the input for this scripts
# run in python3 as follows: python objects_made_2_speak.py < woman.json

import json, sys
import io
import os, random
from time import sleep

# define name export file
filename = "objects_made_to_speak.txt"

# get scraped jsonfile & convert to list of dictionaries
output_json = []
for line in sys.stdin:
	item = json.loads(line)
	output_json.append(item)
length_catalogue = (len(output_json))
#print (json.dumps(output_json, indent=2))

# define list with fields present in json output
fields = ["objectTitle", "collectionName", "dimensions", "objectName", "inventoryNb", "material",\
"dating", "geography", "objectCulture"]

# dictionary with phrases that fit with the fields
sentences = {
"objectTitle": ["I have been catalogued as a ", "I am called a ", "I have been described as a ", \
"Someone wrote I am a ", "A human classified me as a ", "A person portrayed me as a ", \
"I have been painted in words as a ", "I have been depicted as a", "Someone characterized me as a ",\
"The description that was given to me is a ", 'Apparently I am a '],\
"collectionName" : ["I belong to the ", "I have been classified in the ", "Someone chose to file me in the ",\
"The collection I have been assigned to is ", "I have a place in the ", "I have been located in the ",\
"I am situated in the ", 'I can be found in the ', "I am included in the ", "Someone chose to class me in the ",\
" I am categorized in the ", 'Someone decided to list me in the '],\
"dimensions": ["My dimensions are ", "These are my measurements: ", "Someone measured me with the following protocol: ",\
"My proportions are: ", "This is my size: ", "Do you think I am large having a ", \
"I wonder if I would fit in your pocket with the following measurements: ", "My size is: ", \
"In case you decide to dress me one day: "],\
"objectName": ["I am defined as a ", "Generically I am a ", "I am commonly named a ", "My official object name is ",\
"Someone decided to name me as a ", "The name given to me is ", "I am labeled as a ", \
"I have been tagged as a ", "Someone denominated me as a "],\
"inventoryNb": ["I carry the number ", "My ID number is ", "I have been inventorized under the number ",\
"You can identify me in the archive as a ", "One day someone indexed me with the number ", \
"If one day I would carry an ID card, its number would be ", "Translated to an index, I am ",\
"I guess my official name is "],\
"material": ["I am made of ", "My flesh is ", "The material of my body is ", "My bones are of ",\
"I am composed of ", "My essence is "],\
"dating": ["I was made in the period of ", "I date back to ", "I was born in the era of ", \
"Some human must have created me in the epoch of ", "My history dates back to ", \
"My great grand parents must have lived before ", "My descendences goes back to ",\
"I have survived on this planet since ", "I am very very old, from "],\
"geography": ["I come from ", "I was born in ", "My geographical origin is ", "I originally migrated from ",\
"I traveled here from ", "My place of birth is ", "My roots are in ", " I should be able to find some relatives in "],\
"objectCulture": ["My cultural background is ", "I belong to the culture of "]
}


# define number of objects to speak
nr = 3
# in case you want to print all
#nr = length_catalogue

# generate objects data
portraits = random.sample(output_json, nr)

# for each object, create sentences and typewrite them to screen
phrases = []

for n in range(0, nr):
	print ("Object number " + str(n+1))
	d = portraits[n]
	for f in fields:
		description = d.get(f)
		if description:
			phrase = random.choice(sentences[f])
			s = phrase+description.lower()+'. '
			for char in s:
				print(char, end='')
				sys.stdout.flush()
				sleep(0.080)
			phrases.append(s)
		# objects.append(phrases)
	print ('\n__________________\n')
	phrases.append("\n_____________\n\n")
	sys.stdout.flush()

count = 1 
with io.open(filename, "w", encoding='utf-8') as destination:
	#for o in objects:
	destination.write("Object " + str(count) + ":\n\n")
	for p in phrases[:-1]:
		print (p)
		if p == "\n_____________\n\n":
			count += 1
			destination.write(p)
			destination.write("Object " + str(count) + ":\n\n")
		else:
			destination.write(p)
			
# add e-speak