#!/usr/bin/env/ python
# encoding=utf8  

# using scrape.py by Michael Murtaugh, DiVersions, Brussels, 2016: 
# scrape.py generates a json file that is the input for this scripts
# run in python3 as follows: python objects_made_2_speak.py < woman.json

import json, sys
import io
import os, random
from time import sleep
import talkey

# define name export file
filename = "objets_parlants.txt"

# get scraped jsonfile & convert to list of dictionaries
output_json = []
for line in sys.stdin:
	item = json.loads(line)
	output_json.append(item)
length_catalogue = (len(output_json))
#print (json.dumps(output_json, indent=2))

# define list with fields present in json output
fields = ["objectTitle", "collectionName", "dimensions", "objectName", "inventoryNb", "material",\
"dating", "geography", "objectCulture"]

# dictionary with phrases that fit with the fields
sentences = {
"objectTitle": ["J'ai été cataloguée comme ", "Je suis nommée ", "J'ai été décrite comme ", \
"Quelqu'un a noté que je suis ", "Un être humain m'a classifié comme ", "Une personne m'a représentée comme ", \
"J'ai été dessinée en mots comme ", "Quelqu'un m'a charactérisée comme ",\
"La description qui m'a été donnée est: ", 'Apparemment je suis, '],\
"collectionName" : ["J'appartiens à la ", "J'ai été classifiée dans la ", "Quelqu'un a choisi de me ranger dans la ",\
"La collection à laquelle j'ai été assignée, est la ", "J'occupe une place dans la ", "J'ai été localisée dans la ",\
"Je me situe dans la ", 'Je peux être trouvée dans la ', "Je fais partie de la ", "Quelqu'un a choisi de me classer dans la ",\
"Je suis triée dans la ", "Quelqu'un a décidé de m\'inventorier dans la "],\
"dimensions": ["Mes dimensions sont: ", "Voici mes mesures: ", "Quelqu'un m'a mesuré selon le protocol suivant: ",\
"Mes proportions sont: ", "Voici ma taille: ", "Vous me trouvez grosse en ayant les mesures suivantes? ", \
"Je me demande si vous pouvez me filer dans votre poche en ayant les mesures suivantes: ", "Ma taille est: ", \
"Au cas où vous décidez un jour de me faire une jupe: "],\
"objectName": ["Je suis définie comme ", "En terme général je suis ", "Je suis aussi bêtement nommé ", "Mon nom d'objet officiel est ",\
"Quelqu'un a décidé de me nommer comme ", "Le nom qui m'est donné est: ", "Je suis étiquetée comme ", \
"J'ai été taggée comme ", "Quelqu'un m'a dénominée comme "],\
"inventoryNb": ["Je porte le numéro ", "Mon numéro d'identité est ", "J'ai été inventoriée sous le numéro ",\
"Vous pouvez m'identifier dans l'archive comme ", "Quelqu'un m'a indexée avec le numéro ", \
"Si je voulais porter un jour une carte d'identité, il faudrait rajouter une case pour mon numéro, ", "Traduite en index, je suis ",\
"Je suppose que mon nom officiel est "],\
"material": ["Je suis faite de ", "Ma chaire est de ", "Le matériel de mon corps est ", "Mes os sont composés de ",\
"Je suis constitutée de ", "Ma substance est "],\
"dating": ["J'ai été faite dans la période ", "Je date de ", "Je suis née dans l'ère ", \
"Un être humain a dû me créer à l'époque ", "Mon histoire remonte jusque ", \
"Mes grands-parents ont dû vivre avant ", "Ma généalogie remonte jusque ",\
"Je survis sur cette planète depuis ", "Je suis très âgée, de "],\
"geography": ["Je viens de ", "Je suis née à: ", "Mon origine géographique est ", "Originalement j'ai émigré de ",\
"Je suis arivée ici de ", "Mon lieu de naissance est ", "Mes racines se trouvent en ", "Je devrais être capable de trouver des relatifs en "],\
"objectCulture": ["Mon identité culturelle est ", "J'appartiens à la culture "]
}


# define number of objects to speak
nr = 3
# in case you want to print all
#nr = length_catalogue

# generate objects data
portraits = random.sample(output_json, nr)

# for each object, create sentences and typewrite them to screen
# add sentences to list
phrases = []
tts = talkey.Talkey()

for n in range(0, nr):
	print ("Numéro d'objet " + str(n+1))
	d = portraits[n]
	for f in fields:
		description = d.get(f)
		if description:
			phrase = random.choice(sentences[f])
			s = phrase+description.lower()+'. '
			tts.say(s)
			for char in s:
				print(char, end='')
				sys.stdout.flush()
				sleep(0.080)
			phrases.append(s)
		# objects.append(phrases)
	print ('\n__________________\n')
	phrases.append("\n_____________\n\n")
	sys.stdout.flush()

# write to file
count = 1 
with io.open(filename, "w", encoding='utf-8') as destination:
	#for o in objects:
	destination.write("Objet " + str(count) + ":\n\n")
	for p in phrases[:-1]:
		print (p)
		if p == "\n_____________\n\n":
			count += 1
			destination.write(p)
			destination.write("\nObjet " + str(count) + ":\n\n")
		else:
			destination.write(p)
			
# add e-speak