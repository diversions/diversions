Voorwerp 1:

Iemand besliste dat ik hetvolgende ben: naakte vrouw. Ik maak deel uit van de collectie egypte. Vind je me groot met de volgende maten? hoogte: 5 cm, breedte: 4 cm, diepte: 2 cm. Ik werd getagged als een beeldje. Als ik ooit een Belgische identiteitskaart zou hebben, zou men een nieuw veld moeten toevoegen voor mijn nummer, e.03857. Ik besta uit keramiek (aarde > klei). Een mens moet me gemaakt hebben in de tijd van -332 / 395. Mijn roots liggen in productieplaats: egypte (afrika > noord-afrika)
vindplaats: onbekend. Mijn culturele achtergrond is egyptisch. 
_____________


Voorwerp 2:

Ik ben schijnbaar een votiefbeeldje van een naakte vrouw. Ik werd ondergebracht in de collectie egypte. Mijn maat is: hoogte: 6 cm, breedte: 1,5 cm, diepte: 0,8 cm. Ik werd getagged als een beeldje. Ooit indexeerde iemand me met het nummer e.09054. Mijn lichaam is van faience (aarde > klei > keramiek > aardewerk). Een mens moet me gemaakt hebben in de tijd van -1550 / -1069. Aanvankelijk migreerde ik uit productieplaats: egypte (afrika > noord-afrika). Mijn culturele achtergrond is egyptisch. 
_____________


Voorwerp 3:

Ik werd in woorden gegoten als een sierplaatje. Ik maak deel uit van de collectie nabije oosten. Mijn maat is: hoogte: 5,6 cm, breedte: 2,6 cm. Men noemt mij ook domweg een plaatje. Ik draag het nummer o.03478. Mijn essentie is van ivoor (dierlijk > tand > tand van een zoogdier). Ik dateer van -810 / -701. Oorspronkelijk ben ik van productieplaats: nabije en midden oosten (azië)
vindplaats: nimrud (azië > mesopotamië > assyrië). Ik behoor tot de cultuur van fenicisch. 