#!/usr/bin/env/ python
# encoding=utf8  

# using scrape.py by Michael Murtaugh, DiVersions, Brussels, 2016: 
# scrape.py generates a json file that is the input for this scripts
# run in python3 as follows: python objects_made_2_speak.py < woman.json

# inspired by The Death of the Authors - 1941 http://publicdomainday.constantvzw.org/#1941:
# this script generates a .tex file, if you want to convert it to pdf:
# context *.tex

import json, sys
import io
import os, random
import time
from time import sleep
from csv import DictReader

### PUBLICATION ELEMENTS
# Define name file
#title = 'catalogue_nl_stacks'
title = 'catalogue_nl_carmentis'

# Define subtitle of your catalogue
#subtitle = "Exhibition 'STACKS'"
subtitle = "Carmentis.be"

# Define name of your category or collection
collection_cat = "VROUW"


### INPUT DATA
# # import scraped jsonfile & convert to list of dictionaries
output_json = []
for line in sys.stdin:
	item = json.loads(line)
	output_json.append(item)
length_catalogue = (len(output_json))
#print (json.dumps(output_json, indent=2))

# import csv-file & convert to json
# output_json = []
# reader = DictReader(sys.stdin)
# for item in reader:
#     if item:
#     	output_json.append(item)
# print (json.dumps(output_json, indent=2))
# length_catalogue = (len(output_json))

### WRITING
# define list with fields present in json output
fields = ["objectTitle", "collectionName", "dimensions", "objectName", "inventoryNb", "material",\
"dating", "geography", "objectCulture"]

# dictionary with phrases that fit with the fields
sentences = {
"objectTitle": ["Ik werd gerangschikt als een ", "Men noemt me een ", "Ik word beschreven als een ", \
"Iemand besliste dat ik hetvolgende ben: ", "Een mens klasseerde me als een ", "Een persoon portretteerde me als een ", \
"Ik werd in woorden gegoten als een ", "Ik word voorgesteld als een ", "Iemand karakteriseerde me als een ",\
"Dit is de beschrijving die ik cadeau kreeg: ", 'Ik ben schijnbaar een '],\
"collectionName" : ["Ik behoor tot de ", "Ik werd ondergebracht in de ", "Iemand koos ervoor me te ordenen als deel van de ",\
"De collectie die me werd toegekend is de ", "Ik maak deel uit van de ", "Ik werd geplaatst in de ",\
"Je kan me vinden in de ", 'Ik ken mijn plaats in de ', "Ik werd opgenomen in de ", "Iemand besliste dat ik behoor tot de ",\
"Ik werd geklasseerd in de ", 'Iemand besliste me op te lijsten in de '],\
"dimensions": ["Mijn afmetingen zijn ", "Dit zijn mijn opmetingen: ", "Iemand nam mijn maat volgens het protocol: ",\
"Mijn verhoudingen zijn: ", "Dit is mijn maat: ", "Vind je me groot met de volgende maten? ", \
"Ik vraag me af of ik in je jaszak zou passen met deze afmetingen: ", "Mijn maat is: ", \
"Voor het geval je op een dag zou beslissen om een jurk voor me te ontwerpen: "],\
"objectName": ["Ik word gedefinieerd als een ", "In generisch opzicht ben ik een ", "Men noemt mij ook domweg een ", "Mijn officiële voorwerpnaam is ",\
"Iemand besliste me deze naam te geven: ", "De naam die ik kreeg, is ", "Ik draag het label van ", \
"Ik werd getagged als een ", "Iemand bepaalde mijn aard, ik ben een "],\
"inventoryNb": ["Ik draag het nummer ", "Mijn persoonlijk nummer is ", "Ik werd geïnventariseerd met het nummer ",\
"Je kan me terugvinden in het archief als ", "Ooit indexeerde iemand me met het nummer ", \
"Als ik ooit een Belgische identiteitskaart zou hebben, zou men een nieuw veld moeten toevoegen voor mijn nummer, ", "Vertaald naar een index, ben ik nummer ",\
"Mijn officiële naam lijkt me "],\
"material": ["Ik ben gemaakt van ", "Mijn vlees is ", "Mijn lichaam is van ", "Mijn botten zijn van ",\
"Ik besta uit ", "Mijn essentie is van "],\
"dating": ["Ik werd gemaakt in de periode ", "Ik dateer van ", "Ik werd geboren in ", \
"Een mens moet me gemaakt hebben in de tijd van ", "Mijn geschiedenis gaat terug tot ", \
"Mijn grootouders moeten geleefd hebben voor ", "Mijn genealogie gaat terug tot ",\
"Ik overleef op deze planeet sinds ", "Ik ben heel erg oud, van "],\
"geography": ["Ik kom uit ", "Ik werd geboren in ", "Oorspronkelijk ben ik van ", "Aanvankelijk migreerde ik uit ",\
"Ik reisde hiernaartoe vanuit ", "Mijn geboorteplaats is ", "Mijn roots liggen in ", "Ik zou nog familie kunnen terugvinden in "],\
"objectCulture": ["Mijn culturele achtergrond is ", "Ik behoor tot de cultuur van "]
}


# for each object, create sentences and typewrite them to screen
# add sentences to list
nr = 0
phrases = []
for element in output_json:
	print ("Voorwerp nummer" + str(nr+1))
	for f in fields:
		description = element.get(f)
		if description:
			phrase = random.choice(sentences[f])
			s = phrase+description.lower()+'. '
			for char in s:
				print(char, end='')
				sys.stdout.flush()
				#sleep(0.080)
			phrases.append(s)
		# objects.append(phrases)
	print ('\n__________________\n')
	phrases.append("\n_____________\n\n")
	sys.stdout.flush()

# create A5 pdf
def writetoCatalogue(content):
		try:
			logfile = open(filename, "a")
			try:
				logfile.write(content)
			finally:
				logfile.close()
		except IOError:
			pass

# set time & date & filename
title = 'catalogus_nl_'
now = time.strftime("%Y-%m-%d_%H:%M:%S")
# filename = year+'_'+now+'.txt'
filename = title+'_'+collection_cat+'_'+now+'.tex'


# Print header for ConText
header = open('header.txt', 'r')
header = header.read()
writetoCatalogue(header)


# Write catalogue
writetoCatalogue("\chapter{Catalogus\crlf\crlf\n")
writetoCatalogue(subtitle+'\crlf\crlf\n')
writetoCatalogue(collection_cat+'\crlf\crlf\n')
writetoCatalogue('Jubelparkmuseum\crlf}\n')
writetoCatalogue('in het kader van Diversies, Brussel, 2016\n')

# Print text

writetoCatalogue('\n\section{Voorwerpen}\n')
writetoCatalogue('\setuppagenumber[state=start]')

# Open file and add sentences
count = 1 
writetoCatalogue("Voorwerp " + str(count) + ":\n\n")
for p in phrases[:-1]:
	print (p)
	if p == "\n_____________\n\n":
		count += 1
		writetoCatalogue(p)
		writetoCatalogue("\nVoorwerp " + str(count) + ":\n\n")
	else:
		writetoCatalogue(p)

# Open file and add sources

writetoCatalogue("\n\section{Colofon}")


writetoCatalogue('\nDeze catalogus werd gegenereerd op '+now+' van gescraped materiaal van carmentis.be, de online databank van het Jubelparkmuseum.\n\crlf\crlf\nIn het kader van de werksessie Diversies georganiseerd door Constant, in samenwerking met e-Collecties.')

writetoCatalogue('\n\stoptext\n')