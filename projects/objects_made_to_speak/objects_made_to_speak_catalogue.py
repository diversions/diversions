#!/usr/bin/env/ python
# encoding=utf8  

# using scrape.py by Michael Murtaugh, DiVersions, Brussels, 2016: 
# scrape.py generates a json file that is the input for this scripts
# run in python3 as follows: python objects_made_2_speak.py < woman.json

# inspired by The Death of the Authors - 1941 http://publicdomainday.constantvzw.org/#1941:
# this script generates a .tex file, if you want to convert it to pdf:
# context *.tex

import json, sys
import io
import os, random
import time
from time import sleep
from csv import DictReader

### PUBLICATION ELEMENTS
# Define name file
#title = 'catalogue_en_stacks'
title = 'catalogue_en_carmentis'

# Define subtitle of your catalogue
#subtitle = "Exhibition 'STACKS'"
subtitle = "Carmentis.be"

# Define name of your category or collection
collection_cat = "EASTER ISLAND"


### INPUT DATA
# import scraped jsonfile & convert to list of dictionaries
output_json = []
for line in sys.stdin:
	item = json.loads(line)
	output_json.append(item)
length_catalogue = (len(output_json))
#print (json.dumps(output_json, indent=2))

# import csv-file & convert to json
# output_json = []
# reader = DictReader(sys.stdin)
# for item in reader:
#     if item:
#     	output_json.append(item)
# print (json.dumps(output_json, indent=2))
# length_catalogue = (len(output_json))

# define list with fields present in json output
fields = ["objectTitle", "collectionName", "dimensions", "objectName", "inventoryNb", "material",\
"dating", "geography", "objectCulture"]

# dictionary with phrases that fit with the fields
sentences = {
"objectTitle": ["I have been catalogued as a ", "I am called a ", "I have been described as a ", \
"Someone wrote I am a ", "A human classified me as a ", "A person portrayed me as a ", \
"I have been painted in words as a ", "I have been depicted as a", "Someone characterized me as a ",\
"The description that was given to me is a ", 'Apparently I am a '],\
"collectionName" : ["I belong to the ", "I have been classified in the ", "Someone chose to file me in the ",\
"The collection I have been assigned to is ", "I have a place in the ", "I have been located in the ",\
"I am situated in the ", 'I can be found in the ', "I am included in the ", "Someone chose to class me in the ",\
" I am categorized in the ", 'Someone decided to list me in the '],\
"dimensions": ["My dimensions are ", "These are my measurements: ", "Someone measured me with the following protocol: ",\
"My proportions are: ", "This is my size: ", "Do you think I am large having a ", \
"I wonder if I would fit in your pocket with the following measurements: ", "My size is: ", \
"In case you decide to dress me one day: "],\
"objectName": ["I am defined as a ", "Generically I am a ", "I am commonly named a ", "My official object name is ",\
"Someone decided to name me as a ", "The name given to me is ", "I am labeled as a ", \
"I have been tagged as a ", "Someone denominated me as a "],\
"inventoryNb": ["I carry the number ", "My ID number is ", "I have been inventorized under the number ",\
"You can identify me in the archive as a ", "One day someone indexed me with the number ", \
"If one day I would carry an ID card, its number would be ", "Translated to an index, I am ",\
"I guess my official name is "],\
"material": ["I am made of ", "My flesh is ", "The material of my body is ", "My bones are of ",\
"I am composed of ", "My essence is "],\
"dating": ["I was made in the period of ", "I date back to ", "I was born in the era of ", \
"Some human must have created me in the epoch of ", "My history dates back to ", \
"My great grand parents must have lived before ", "My descendences goes back to ",\
"I have survived on this planet since ", "I am very very old, from "],\
"geography": ["I come from ", "I was born in ", "My geographical origin is ", "I originally migrated from ",\
"I traveled here from ", "My place of birth is ", "My roots are in ", " I should be able to find some relatives in "],\
"objectCulture": ["My cultural background is ", "I belong to the culture of "]
}


# for each object, create sentences and typewrite them to screen
nr = 0
phrases = []
for element in output_json:
	print ("Object number " + str(nr+1))
	for f in fields:
		description = element.get(f)
		if description:
			phrase = random.choice(sentences[f])
			s = phrase+description.lower()+'. '
			for char in s:
				if char == "#":
					s = s.replace(char,'')
				print(char, end='')
				sys.stdout.flush()
			phrases.append(s)
		# objects.append(phrases)
	print ('\n__________________\n')
	phrases.append("\n_____________\n\n")
	sys.stdout.flush()


# create A5 pdf
def writetoCatalogue(content):
		try:
			logfile = open(filename, "a")
			try:
				logfile.write(content)
			finally:
				logfile.close()
		except IOError:
			pass

# set time & date & filename
title = 'catalogue_en_'
now = time.strftime("%Y-%m-%d_%H:%M:%S")
# filename = year+'_'+now+'.txt'
filename = title+'_'+collection_cat+'_'+now+'.tex'


# Print header for ConText
header = open('header.txt', 'r')
header = header.read()
writetoCatalogue(header)


# Write catalogue
writetoCatalogue("\chapter{Catalogue\crlf\crlf\n")
writetoCatalogue(subtitle+'\crlf\crlf\n')
writetoCatalogue(collection_cat+'\crlf\crlf\n')
writetoCatalogue('Musée du Cinquantenaire-Jubelparkmuseum\crlf}\n')
writetoCatalogue('in te framework of DiVersions, Brussels, 2016\n')

# Print text

writetoCatalogue('\n\section{Objects}\n')
writetoCatalogue('\setuppagenumber[state=start]')

# Open file and add sentences
count = 1 
writetoCatalogue("Object " + str(count) + ":\n\n")
for p in phrases[:-1]:
	print (p)
	if p == "\n_____________\n\n":
		count += 1
		writetoCatalogue(p)
		writetoCatalogue("\nObject " + str(count) + ":\n\n")
	else:
		writetoCatalogue(p)

# Open file and add sources

writetoCatalogue("\n\section{Colophon}")


writetoCatalogue('\nThis catalogue was generated on '+now+' with material scraped of carmentis.be, the online database of Musée du Cinquantenaire-Jubelparkmuseum.\n\crlf\crlf\nIn the framework of the work session DiVersions organised by Constant in collaboration with e-Collections.')

writetoCatalogue('\n\stoptext\n')