Object 1:

Apparently I am a "toki". I have been located in the collection oceanea. Someone measured me with the following protocol: length: 24,5 cm, width: 12 cm, height: 9,5 cm. I have been tagged as a pick (stone tool). One day someone indexed me with the number et.35.5.94. My bones are of basalt (stone). I am very very old, from before 1934. My geographical origin is place of production: easter island (america > south america > chile > valparaíso (region)). 
_____________

Object 2:

The collection I have been assigned to is collection oceanea. In case you decide to dress me one day: length: 14,7 cm, width: 6,4 cm, height: 5,7 cm. Generically I am a pick (stone tool). If one day I would carry an ID card, its number would be et.35.5.93. The material of my body is basalt (stone). I have survived on this planet since before 1934. I originally migrated from place of production: easter island (america > south america > chile > valparaíso (region)). 
_____________

Object 3:

Apparently I am a "hau maroki". I am included in the collection oceanea. My size is: height: 21,1 cm. I have been tagged as a hat. If one day I would carry an ID card, its number would be et.35.5.266. My bones are of chicken feather (animal > feather)
fiber (vegetal). I have survived on this planet since 1900 / 1934. My roots are in place of production: easter island (america > south america > chile > valparaíso (region)). 