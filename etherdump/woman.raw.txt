About politics of representation of women on Wikipedia
08.12 Cath, Andre, look at  Wikipedia API to collect information on Drawings Portraits or Women

Media in category "Portrait drawings of women" : The following 70 files are in this category, out of 70 total. 
https://commons.wikimedia.org/wiki/Category:Portrait_drawings_of_women

Media in category "Portrait drawings of men" : The following 200 files are in this category, out of 214 total.
https://commons.wikimedia.org/wiki/Category:Portrait_drawings_of_men

Scraping from Wikipedia using API
https://www.mediawiki.org/wiki/API:Main_page
going into the sandbox
https://www.mediawiki.org/wiki/Special:ApiSandbox
also look at André's article: http://publicationstation.wdka.hro.nl/wiki/index.php/Courses/Wikis-Publishing_Platforms#Mediawiki_API

MediaWiki Client
mwclient is a lightweight Python client library to the MediaWiki APIwhich provides access to most API functionality.
https://github.com/mwclient/mwclient

Scripts & results
See mw_client_cats.py & txt file result.

import mwclient
from datetime import datetime

category = u'Portrait_drawings_of_women'
# Portrait drawings of man

site=mwclient.Site('commons.wikimedia.org')
cats= site.Categories[category]
pages_under_cats = list(cats.members())
print len(pages_under_cats)
files_under_cats = [page for page in pages_under_cats if page.namespace == 6] #takes only the files'pages
for item in files_under_cats: #gets the info from each files'page
        name = item.name
        media_url = item.imageinfo[u'url'] #file url (where the file is stored)
        print name, media_url

About politics of representation of women on Wikipedia
09.12 + 10.12

Loraine shows : 
https://en.wikipedia.org/wiki/Wikipedia:Writing_about_women
https://en.wikipedia.org/wiki/Wikipedia:Essays

Start writing an 'essay' page on Wikipedia to problematize & share these reflections.
https://en.wikipedia.org/wiki/Wikipedia:Visual_representation_of_women


