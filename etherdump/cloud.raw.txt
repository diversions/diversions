Cloud solutions for museums

Decolonization discussion with Zoumana, Lionell, Seda
http://diversions.lan/pad/p/decolonize_ZLS

Questions:
    Transition to Museum+RIA? Whose the IT personnel now, can we talk with them? Which modules did they purchase? What's agreement on the data? What's the format, what do they with it?
    How was the decision made for the software? What's the vision, does he know?
    Belgian company and what happened there? Could they salvage anything? Was there software beforehand? 
    Are they any standards that they use? 
    How do they decide on the fields? Is it decided between It, collections and e-collections? What are the international relation of structuring collections?
    What they had locally and then had to teach the CMS to do? Or, training for curators? Any sort of troubles...
    Multilinguality and software?
    Inputing relations between objects is difficult, are there similar problems?
    Collective access: is used a lot in Belgium but by flemish institutions (CMS). Have they heard of it before? 
    
    
*

Holy Trinity:
consumer, producers of content, infrastructure
An important step to cloud services


Overview:
    https://en.wikipedia.org/wiki/Collections_Management_System vs https://en.wikipedia.org/wiki/Content_management_system
    
From MuseumPlus RIA:
    The SaaS option allows us to offer you an optimal and cost effective system if any of the following applies to you:
* No wish to buy software. This means a low initial investment.
*Limited budget for IT infrastructure
*Limited IT resources and no expansion planned
*Plans to outsource IT services

Adlib 
is specifically designed for recording and managing your Museum’s collections data. 
http://www.adlibsoft.com/ http://www.adlibsoft.com/products/museum-collection-management-software (what are the cloud differences between library and museum..)

Keepthinking 
The universal information management solution 
http://www.keepthinking.it/ 

TMS 
Helps You Expertly Manage Your Collection 
http://www.gallerysystems.com/products-and-services/tms/

MuseumPlus ?
http://www.zetcom.com/en/products/

If you want zetcom to host MuseumPlusRIA, we offer it as Software as a Service. (The customer may opt to run his own servers).

MuseumPlus RIA
http://www.zetcom.com/en/products/collections-management-software/
http://www.zetcom.com/en/products/museumplus-ria/highlights/
http://www.zetcom.com/en/products/museumplus-ria/software-as-a-service/

MuseumPlus RIA license purchase price for local installations start at £ 1,530  (€ 1,880) for the standard version. (Per user)
MuseumPlus RIA SaaS subscription fee (hosted by zetcom) start at £ 49  (€ 60) per concurrent user per month for the standard version. Included in the subscription fee are hosting, server, disk space, maintenance, support, hotline and updates. No extra support and maintenance contract is required.

ArchiveMatica
https://www.archivematica.org/en/
Ben Fino Radin
http://benfinoradin.info


CollectiveAccess
http://collectiveaccess.org

----------------

Cloud solution for libraries

Adlib
is a professional grade software package for information, knowledge and catalogue management in libraries, resource centers and similar organisations.
 http://www.adlibsoft.com/products/library-software

Aura 
Online software voor uw bibliotheek
http://www.aura.nl/Home.aspx http://www.aura.nl/Auraproducten/AuraOnline.aspx


---------------

Texts/Papers

INVENTORY, ACCESS, INTERPRETATION: THE EVOLUTION OF MUSEUM COLLECTION MANAGEMENT SOFTWARE
http://library2.jfku.edu/Museum_Studies/Inventory.pdf

Telling Stories: Procedural Authorship and Extracting Meaning from Museum Databases
http://www.museumsandtheweb.com/mw99/papers/dietz/dietz.html

http://rcaam.org/cms/wp-content/uploads/2011/05/Graduate-Studies-Thesis-Collections-Management-Elana-Carpinone.pdf



The Culture White Paper
"Technology offers many opportunitiesto bring our culture to many morepeople in many different ways. Wewill work with our cultural institutionsto make the UK one of the world’sleading countries for digitised publiccollections and use of technology to enhance the online experienceof users."
"Many museums are actively involved in digitising their collections, but still only a fraction of the extensive and unique collections of our national museums can be readily viewed by the general public. The governmentis providing £150 million of capital funding over the next five years to the British Museum, the Science Museum and the Victoria and Albert Museum to preserve, protect and transform public access to the collections currently stored at Blythe Housein London, by relocating them to appropriate, world-class facilities.As part of this move, objects from the collections will be photographed and made available digitally."
https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/510798/DCMS_The_Culture_White_Paper__3_.pdf


Standards:
    
Conceptual Reference Model
http://www.cidoc-crm.org/get-last-official-release
Old CIDOC:
http://old.cidoc-crm.org
    

(Industry) Events
*
Collective Imaginations 2016

*http://www.gallerysystems.com/ci2016/ci2016-schedule/?mkt_tok=3RkMMJWWfF9wsRomrfCcI63Em2iQPJWpsrB0B%2FDC18kX3RUsKruWbQfind1SFJk7a8C6XFZDQ91M4jQVQ7jM
*Twitter stream:
*https://twitter.com/search?q=ci2016ny&src=typd
*Report from Gallery Systems:
*http://www.gallerysystems.com/its-a-wrap-collective-imagination-2016/
*
Collections Trust Conference 2016
*http://www.collectionstrust.org.uk/events/item/13967-conference-2016-presentations

How to chose a CMS?
*http://www.collectionstrust.org.uk/collections-link/collections-management/spectrum/choose-a-cms
*http://www.collectionstrust.org.uk/collections-link/collections-management/spectrum/spectrum-resources/item/13731-choosing-a-cms-advice
*
Registrar's Committee of the Alliance of American Museums Listserv
*http://www.rcaam.org/listserv


//

historical diversions

//

Previous links/Resources


Amazon AWS:
*    user: diversions@yopmail.net
*pass:istooshort
*https://aws.amazon.com/premiumsupport/compare-plans/
*
Open Guide (not a tutorial)
*https://github.com/open-guides/og-aws#scope
*
Infrastructural stuff..?
*https://telecominfraproject.com
