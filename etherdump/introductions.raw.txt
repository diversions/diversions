Monday 5-12

Presentations round

Seda
temporarily works with Constant, was in NY last 3 years
computer scientist, researcher at university, on privacy & surveillance
trying to get a critical stand on what computer scientists do
several projects: 
    - inventory in the home / bring out the conflicts vs 'privacy does not deal with conflicts'
    - histories of big data vs 'big data comes out of nothing'
    
mention of http://meson.press/books/there-is-no-software-there-are-just-services/
development of the cloud: industry wants to maintain software cost lower / harvest data -> cloud

plans for DiVersions: a way to touch the cloud, to see MS/Amazon/Google as versions of the cloud
racial critique/war-on-terror critique as part of the critique on surveillance.


Femke
she works for Constant, she's a graphic designer
software and practice
started project on 3D - there is something going on there on the math, tools, software
meeting between military, Hollywood, plastic surgery,  gaming that ask for attention.
DiVersions: 
versionsing practices:  popularity of Github, shift in software ethics. forking: would be the last resource, whereas in github it just became common practice
where does conflict go?

Kristien
knows Constant through working with Active Archives, was developed further with Alex (OSP) and Sarma: http://oralsite.be/pages/Olga_Software
Olga
project is still running, & many questions in difference regarding authoring, publishing
recently a lot of  cooridnation work: distande towards research, but good to reconnect through the worksession
own research is on chance support: what is the (im)proper material you're working on? Concept from Georges Perec, Species of Spaces: the space of the page
Writers who has used chance supports to write
Archiving logics for writers and artists, digitization of these archives - what transitions/modifications are made to the material?

Wendy
she works for Constant
this Thursday opening at Constant office in Saint-Gilles from 18:00: everybody's welcome :-)
Objects in commons:http://constantvzw.org/site/Libre-Objet-Peggy,2688.html?lang=en
fablabs 'share what's hapening there' ><fablab as a service
Libre Objet designed furniture called 'peggy' which is executed by different labs, under different circumstances and materials: 
http://libreobjet.org/objects/peggy/project.html
personal work: makes 'akward' interfaces', slow things down
Next year: research on coulour, skin colour & textiles

Nicolas
member of Constant for 25 years....... ;-) not 15.
With Michael will present together tomorrow, 
research projects under the name 'active archives' 
http://activearchives.org/
Doing Phd research on computer vision and the relation of computer vision with photography.
How did we get to this idea of computer vision.
discovery: computer model based on vision of frogs, interesting position between human/animal/machine
moving from algorythms to frogs. A third interlocutor in the room: a animal that talks about the vunerability of the flesh.
history of violence has been imprinted in these tools, with the way we test or develop these technologies
How can we think different of science that is not part on this history of violence. 

DiVersions: how do we divert the track of these machines to other beings?

Gottfried
participated in Pipelines worksession: http://constantvzw.org/site/Worksession-Promiscuous-pipelines.html?lang=en
software tool builder, educator + artist
worked a lot on 'unboxing': what is there when you ogpen up the system
constructing a counter narrative to tron like 0s and 1s story of digital tech
analysis of source code, and language that is used
worked on hotglue
code as a way to conceal labor (of developers): 
DiVersions: INC Publishing Lab: combine collected texts on same level (source code/text) - continuous integration
whenever you push a document, things get automatically rebuild
it allows for algorithmic writing or writing with algorithms
also curious to look at collection

Martino
I do research, general approach
graphic design in Amsterdam, started a hacklab experimenting with the political aspects of technology
moved to Brussels, studies philosophy
collaborates with Constant regularly
general interest: technology as a site for certain political tentions. How technology functions, what it means for it to function? Example:  has been looking at obsolete network protocols
with Roel Roscam Abbing
what does it mean for a protocol to function as a way of understanding how the internet works?
we looked at protocols prior to TCP/IP
70s/80s two proposals on network communication: we read them from a polarization perspective, eu vs. us, military vs. public vs. private networks
started using term 'legacy': a term used to mention obsolete tech that still need care and maintenance
unpack histories of power and economy that are coming with the technology.

DiVerions: look at classification as a way to look at legacy of the 'library/collections'

An
working with and for constant since 2007 had a research question in her desk: "what means to be a writer in the digital age?"
Algolit - collective research on algorythms and literature.
OuLiPo as basis.. conditions for potential literature. http://oulipo.net/
In last years have been using machine learning for   ??
part in digital environment, part in the forest. 
underlying statistical models shape how we work today, based on different assumptions
novel in diffs

Sam
from the video side of things: http://www.cameralibre.cc/
pushing the ideas of FLOSS outside software areas.
artistic and propagandistic
Make video for organizations that default to open source, open data, 
open source circular economy days: https://oscedays.org/
into animation lately. more flexible and more possibilities in open source.
focus on the public domain, applied to daily life
remembrance of lost species day:  http://www.cameralibre.cc/huia/
drawing of a dead bird that was sent to london + whistling of a guy remembering what the bird sounded like 40 years after extinction!
trying to bring it back to life

Zoumana
doing forum theatre
a show about specific problem, the show represents a conflict and we ask spectators to come on stage and find an issue
this tool is not for solutions, but to open the possibilities in a situation
to have more data on the different elements of the situation
to create nuance around the conflict and create a larger view of the situation
started apass as participant: http://www.apass.be/
create a tool to generate conflict (in fiction). conflicts generates concerns.
tool is not just there to measure the space of concern but generates a space too
obcessions on imagination. Imaginations is influenced to technology. I want to build an aparatus to explore how our imaginations is full of this things.
phantasy: one part of the description of the spirit, how perceptions allow us to believe in something, i can see a shadow and believe that it is a person

Adva
Dance and performance background. a few of the connection between her practice and what will be happening here.
researching relationship between the body and digital, how digital representations may lead to generating new understandings of the body and movement
one of this process is happening with Femke:
Also with OSP, worked with Alex on a collaborative program
collaborative practices i developed 7 years ago: i was busy with questions of authorship and collaboration, what does it mean to work with material someone else left for you or before you
this was not with digital material but i think these experiences can feed each other
ao : http://www.dominokingdom.be/
as a teacher: approach scool environment as collaborative environment (non-hierarchical, where everybody is involved in decisions and proecesses)

Alex
in between graphic design and programming.
work with OSP http://osp.kitchen/
9 members, started in 2006
OSP started when Macromedia was bought by Adobe, and there only one company controlling the tools for graphic design.
FLOSS was a way to get in touch with a materiality of the tools.
independent organisation since 2 years
we try to find a balance between research, education and commissions
we are also looking into ways of making our group sustainable and share with others the things that we work on as a structure
read and write platforms: collaborative platforms to take notes at an event like this in order to make a publication
How we these tools can write stories?

André
came to F/LOSS through sound art and experimental music and generally publishing
develop tools and workflows in an educational context: 
Piet Zwart Institute:http://www.pzwart.nl/media-design-experimental-publishing/
timestamps/elements that are normally hidden, how can we bring these to the foreground as part of a publication
involved in an archive project coordinated by Renee Turner called The Warp And Weft of Memory http://castrumperegrini.org/2016/03/08/the-warp-and-weft-of-memory/ ? (yes!)
working with a semantic mediawiki; still processing the consequences of this, such as the serendipity and the potential to get lost through semantic content
all of the archive is grey zone, no green zone

Myriam
part of collective Just for the Record
visual artist, working with analogue tools (painter, ceramics)
works a lot with 'mould', archaic tools for duplicating - modifies & recombines objects
works in art school, reorganises books in the library, tries to buy more books about women/non-european artists

Following presentations
http://diversions.lan/pad/p/just_for_the_record
http://diversions.lan/pad/p/conflicts_and_confusions


EVENING SESSION

Peter

with Constant since 2003
background in visual arts, mainly in public space
Different settings : 
*-  how you move through the city
*- how the city relates to social routines 
      - poetics around languages, hybrid languages, everything outside of normative language
Since 2009, we collected w/ constant neologisms in neighbourhoods

http://www.constantvzw.org/site/-Parlez-vous-Saint-Gillois,187-.html
scharbeek ?
idea was to make a portrait of brussels as multi-cultural city
using broken words, not-functional language
Looking at it as expressive or aesthetic material
series of activites - 80 in 2 years, recording audio
Two databases :
    - Parlez vous 1060 (http://parlezvous1060.be/)
    - http://www.lalangueschaerbeekoise.be/w/Bruxellois-e
interested in making verbal negotiation around words that you use visible, including accents, coughs, etc

The idea of a dictionnary can both be descriptive and  instructive / prescriptive
"who has the right to compile these rules of behaviour?" is an interesting question
How can these projets can be source material for new ones. How langage can ben versionned , sub-versionned 
interested in how language can be divergent, convergent, invergent, etc

Sarah

part of Just for the Record
And Graphic Designer at OSP, a collective where design is made w/ open source software
http://osp.kitchen/
OSP do commisions, research and workshops
we want to find ways and develop tools to work together, and work with those who commision OSP, and involve them in the production and development of the project
We developed some tools (html2print for example), which is a way for us to be able to work on the same file (compared to usual design softwares).
you can make a collaborative file, eg pads, rather than working each on your own file individually, thus involving the collaborator.
there's a strong educational part to this, including the client in the process is part of this philosophy.
With Medor ( https://medor.coop/fr/ ) for example, is a common-space to discuss, to have conflicts maybe, and this space becomes the site of the proect but also the project itself
we need to find ways of discussing and solving conflict without traditional hierarchies.
Medor was an interesting project in that we worked with 18 people, including journalists who didn't have experience with open licenses etc.
HTML2print is avalaible online, but it's not a "software" and you need to be used to html and web programming to use (https://github.com/osp/osp.tools.html2print)


Catherine
we have different versions of our work depending on the context, this is tonight's version:x
my practice lies in collaborative practice, offline networks, archive, classification, nomenclature, so it's a good fit with DiVersions
busy the last 4 years with research and fiction project around Anna Kavan  https://en.wikipedia.org/wiki/Anna_Kavan
My research in fiction with this authour took many forms : online (http://kavan.land/), and offline (http://kavan.lan, through piratbox), and a  book is called Anna K.
Anna K. tells the story of writing a divergence story about a writer who has been buried by writers and critics - the paradox is a forgotten and cursed writer, but there is a lot of material online.
Two biographies of her were written about her,  Catherine then realised that she needed to write her own version - the biographies were one-sided, written by men, sensational and focused on her life, not her writing.
She writes auto-fictionnal works, and re-develop plots years later … In that sens she has a versionned way of writing.
the more you read Kavan, the more you know about her life - different versions of her life.
working with local networks, offline libraries, she has an interest in Local Area Narratives.

Philip
Architect and researcher based in London
I'm interested in studying the 3D software, 
it's an interesting moment for architectural software, still desktop-based, but pressure pushing towards the cloud.
From both government and big companies willing to put them online.
I'm studying, done projects about 3 aspects of this software:
    the geometry - vertex, mesh, object etc, including critiquing the MakeHuman project
looking at file types, eg encoding 3d geometery into pixels, so you can for example tweet a 3d file.
The complexity of 3D model that are generated, databases are not just design tools but rather assets that are handed over to the owners of the building. a very rich archive.
interesting differences between the object described by software, the object itself, and its metadata.


Cristina

Background in graphic design.
Studied at Piet Zwart Institute. 
Graduation project on Bots :
"Bots in Wikipedia:" collaborative work between humans and bots. 
15 years of history.
Memory technologies, timestamps.
Research interest: 
- how bots change the fabric of the community. 
ie: authority related to these bots - bureaucratic structure, competitions between human editors and bot editors,
bots have an account & charistmatic persona , praised by people,  bots sometimes have more hierarchy in user rights than human editors. 
Look at "bots retired", re-enact the their performativity, check their edits;

- the visual language that is a big element in the wikipedia bots. 
ie: the war related imagery,  the specific atmosphere, 70% images are connected to the military narrative. 
makes an analogy with the "edit war". is both in the language and in the image. Also images of cats, dogs, eagles.
Q: does they change the images when they retire ?
*ccl hijacking the notetaking* forgot to mention: currently working with PublishingLab on a tool to gather Wikimedia content and place it in an ebook. taking an article apart using the API and restructuring it. metaphor of the scrapbook as personal archive. escaping the necessity of usefulness present on wikimedia through the tool into the private space of the book


Martin

From Marseille, lives in Bxl, study Graphic Design and now new media at ERG (OSP teacher).

Interests
- making his own tools.
ie: A copy of html to print tool, "in my own way".
- How services on line, like the google hegemony, shape the use of the internet in a quiet way. 
ie: Materialization of image search in google. Make tangible the products for people to understand the absurdity
Learn the grammar of smart objects like the "smart object" and all the narrative of innovation given by corporations who have a specific grammar and
lexicon. 
- Interest in archives, discover "dépôt légal" at KBR (Royal Library), a place where you bring a published book.

Magnus

"I love recycling" so this is why he starts reading his application.

[ copy paste here from his application ] thx.  

ML: edit my own entry? Remember also the last minute of the presentation, including twenty years working in and around hacklabs and artist-run spaces :-)

Presentation..here's the short version:
                                
version control enables technical and social practices (modes?). 

Distributed version control software and services, accept simultaneous requests from multiple time zones and from human and non-human agents alike. 

In social media the same infrastructure enables a constant flow of new and revised (curated) versions: a global supply line for live round-the-clock news and current
affairs

Histories of collaboration and sharing bring examples of sharing, in gift exchange, FLOSS tools and time-sharing computer systems: self-institution through learning (in Derrida's terms, counter-institution and 'difference').

Chris Kelty's ethnography 'Two Bit' shows impact of 'recursive geek publics' on the design and implementation of Git, and the Linux distribution project, where recursion reflects politics and governance in free software, and shows how decision-making procedures can endorse diversity, open-endedness, ambiguity; invoking self-actualization, through boot-strapping pedagogical practice.

Narratives of rivalry and heroism common in the portrayal of hacker culture (Levy)  But the same conflict must not necessarily be the subtext to contemporary versionsing.  Instead, code comments, commit messages, and conversations behind public facing wikis present a variety of ways by which to resolve differences without flames.  

METHODS WITHOUT METHODOLOGY

Concerning my PhD experience: In doctoral programmes, opposition and adversity can appear to be immutable functions, where closure comes only by a defence of 'practice-led' or 'by research' methods. What methodological alternatives are implied by a politics of the merge? Using version control, I aim to make the production process visible in gitlab. This might bring questions about authorship, collaboration, and the collective imagining and creation of versions in the
marriage of fact and fiction. 

In committing this thesis-experiment to my own archive, eventually to be
disemminated through my University's research portal, I wonder what would be the
equivalent to a situationist inspired, frictional, sandpaper bound text? The use
of - and aesthetic engagement with - UNIX, is a beginning; a step towards
disordering a set of methods without methodology. In formulating such a
counter-ethic, Derrida's work is again relevant - to discussion of archives, in
ethical and legal terms, and considering themes of distribution and
transparency - in images, software, societies and organizations.

Donatella

Work for Constant since 5 years
Literature & language as background. Work as cultural manager, has worked in different institutions.
Master studies in Museology but never work in a museum. 
She ended up in the question of archives often 
Has worked at Argos (they work on video archives).
How far you can go with this archives, lots of questions not always tackle in the best way from her point of view.
ie: you can look for names but you cant watch videos on line.
She ended up in a private gallery and it as "A nightmare.".. literally a nightmare 
Huge collection : artworks moving a Keith Haring and Magritte and shaking 
but also they have a collection of archeological objects.
objects with values - related to auction sales.
Purely commercial and no contextualization at all, so she respects what museums do because in relation to galleries
their contextualization is needed. 
Museum vs private gallery. responsability & ethics.
In Constant she takes care of the organization and communication and administration. 
*Femke: archival processes. 
Daily work as an archive of Constant - the archivst of the organization. 
Board member of JFTR.


Julie

comes from a fine art background, she doesn't fit the categorisation
bachelor from erg 
pietzwart instituut
artistic practice of making websites
geographically making sense of her relationships with different computers
experimenting with geolocating databases
locate geographicly places and ph
made plugins to enable you to play with geolocation and have a physical representation of the websites you are visiting
used panoramio
visual representation of the website
getting acquainted with the GeoIP database of MaxMind (used to detect fraud)
graduation project earth drawing of constellations through IP addresses
star constellations used to orientate yourself in old civilisations -> web navigation
going to places through internet, basicly always going to US
interested on preservation and documentation of art work
works that are emerging from digital environments are comparable to works from land art
issue with migration of ammunition?
documenting or copying or documenting making circulate files as preservative strategies
documented a JODI performance at Stedelijk
how can the encounter with digital art be seen as a particular moment? 
treating it as an event could bring better preservative startegies in order to capture those works
documenting the documentation process
capacity of copying the file and making it circulate
enable people to see the variety of accounts
reinterpretations of work

Lionel

close to Julie interrest of preservation
do research in preservation of digital work
teaches media archaology and does preservation of digital artworks (pamal.org=place he works)
try to experiement reconsruction of digital work with original material such as hard ware and softaware
3D software made in the 80s. Annie Abrahams owner of the software. They tryed to rebuilt the software on an amiga
exhibition of this remake, version of the original software and the original
everyone can experiement on the amiga + online version
reconstructed old website html
for example the one of Gregory Chatonsky
they call these reconstruction second original
http://pamal.org/wiki/Accueil


Mia

part of just for the record
lives in Brussels
visual artist not from ogramming perspective but more on object like sculpture
sculptor
dabbles in construction work
comes from an education of product design. how do we read objects in relation to ourselves as physical entities? what about permanence and size?
how we read tools as physical construction. Like tools are made from several different size and mold/hands
intersted in language and logmaking
1st encouter with log during a sailing travel where sha had to log whch did create narration and gave a tr of the climate
collections as such where the collection 
looking at the semantic framing
smtg that shes been thinking about jftr but also now with diversion in museum narration
for this session she is interested in rewriting things that are considered taboo in our society
what is considered as taboo change so much. Quick rewriting that happen in a really short amount of time
attack the idea of taboo
ship log is the book of the ship narrating the structure and story of the ship through every sailor ship and travels (like the ship diary)
sometimes the lack of emotion in ship logs can be heartbreaking (eg war ships where the only way to tell what happened is to see the difference in the number of sailors who have died and those that survived)

Arianna
Is a waitress an dartist researcher at a.pass 
participatory - project based.
she has a project in collective apass environlent
research is based on
the ghost
(the host
the cost
the coast ? )
daily gesture of care taking 
assigned to the feminine gender
sollidified in caretaking practices
now this is embodied

research based in my model gestures in daily caretraking
gathering in/of material gestures

because she works with family / in her own lineage -> intimate relation -

At this moment:
( ... problematising .... )

"why i fealt allowed to approprate these gestures?" - " Although the gestures are also in me, as I learned these gestures"
"what are the means?"
"how am I gathering the material?"

The gesture of making an arwork is also a gesture on the gestures
(the recording is made by video)

how does intimate space alow for appropriation, hiow can this be shared under which circumstances, what .. grounds

(keyword intermezzo: )
... body 
... maintain archive of gesturess by doing them daily
art ...
mine ...
i do ....
:-) ...

physical and digital at the same time
why in unfamiliar space (prrrr ... ?)

she records the gestures in video and interviews

Marie - soon 30 
2
tuirning 30 on saturday
Saint-gillies
Bergmanstraat 77 top floor (bring champagne and gifts !) (and compliments)

Background in graphic design
in Brussels for 8-9 

interests:
cycle of creation, conservation archivation, circulation
reproduction
publishing 
something is never original
each catalogue is a new work
when how what do we call a copy ? 

She works Royal Library ...
she does project 'parallel libraries'
balancing art design heritage and library
connection design schools and archives

                                
Accessability
the cycle of publishing of documents –the circulation is interesting as one 
k
with the archives

reproduction
it is always
never original never perfects

came to diversions
when and how are we making a copy, whatis a copo
how does it change the narrative

half of the week working in an archive
half of the week working with La Houle– publishing house 

interest in public libraries – forarts and design projects alike into library
when there is more heritage in culturalreproduction


Lauren


erg – worked with books, paper books,
not digital – but paper books not herthing
I met sarah when she did an internship at OSP the first time to work through the approach of philosophy
first time I met FLOSS tools she introduced me to the floss philosophy
then I met Eric, who became my boyfriend
he could really accompany me, but then said: you can do your own website, and I discovered that it was amazing
I thought waw that is so cool I really want to make it my thing. 
Somehow I became a feminist. 
I slowly walked into it
I teach ... realized she could not continue to tell things the same way (without a feminist perspective) In the school there are a lot of narratives that aremissing. 
As well as with the recent events onthe international scene the need to do this work has 
biassed ... 
meet new narratives
feminism and graphic design
engaged politically
just like the



Geraldine:
    
G thinks that sometimes feminism can be an alianating force
Constant feels like the only space that is not alianating 
:-)
Would not call herself a feminist, but supports the work and is interested in the ideas
What it is ? 

Has a practice in fine arts 
Through that doing open source - "jumped on the bus of Open Source and public domain" - but then needed time to start questioning 

Never had time to reflect on it, 
and at some point 'I got invited to Constant' 
Technology is not the center; but the context and the people are
Archive ... can be a box with things, but it can also be opening new perspectives
How did we end up with these categories
Interested in the social process of accounting
epistemic systems
not give more agency then needed to the tools, but more t

Information and technology - wants to learn how these categories of open knowledge and started to come about. 
English language also an example: "research" - looking into something

When are things knowledge and when are they just data

I am trying to privilege poetics
technically impressive is not catching on anymore

I am working with noise
the voice as a mediation device
Because I am so hyper and stufff .. :-)

