Questions session with Nacha (Thursday)


advanced search: lace collection / image yes
Questions of translation,
No English title, falls back to French

Example of "pottekant"

Inventory number in the images to be able to reference it.
TIechnique shows all the different kinds of lace making
curator of textile wants catalogue number inside the picture

Advance search to search directly in the Thesaurus
Can specifiy Material/Technique and the field has autocompletion with the Thesaurus.

Samples & samplers.
YES samplers have a keyword / category.: go into advanced search, object name 'sampler' or 'swatch'
Sampler (there are 19 of them)

Object Name contains it
Lace samples, echantillon?
swatch
also try to look for 'textile' and see what comes 

Title vs. Name: for instance:
Object Title: Mona Lisa
Object Name: Painting

So Object names are a controlled vocabulary.
They make their own -- have an old version
They use "AAT" The Getty thesaurus to base their vocabulary.
http://www.getty.edu/research/tools/vocabularies/aat/

Project about fashion Europeana
Lace from KMKG on europeana:
http://www.europeana.eu/portal/en/record/2048204/RMAH_204279_NL.html
look for Fashion

thesaurus was intended to be 300 words long, ended up being 2000, in 13 different languages (ao hebrew)
gender is part of fashion collection
no gender in catalogue, not demanded / not interesting enough
keywords: colours, shapes, iconography (male/female)
old version of theasurus

Marie: How do you deal with situation when you don't know exactly (maybe / peut-etre)
We don't allow "Maybe" in the object name...
Either pick a broader term (more correct)
If a name doesn't exist, tell curators to go into historical terms
Sometimes going broader in geography becomes "the world" -- not useful
Allow two terms runs into problems (esp. Geo / Object name -- must be one)



more common with geographical references: both current and previous locations in the title

Michael: What kinds of trouble does it cause when there is 2 terms
technical difficulties, not conceptual problem / provider (the Swiss) company tells them it is impossible

Sam: connect to other online catalogues?
Japanese prints published on Carmentis, in process of being imported in ukiyo-r.org
tries to be complete database of woodcarves Japan
Japanese prints: published on caramentis.be, as well as 
S
ukiyo-e.org database (independent) project created by John Resig (who has recently become visiting researcher at Ritsumeikan University in Kyoto).... John Resig is also the creator of http://jquery.org/ !
example of the same print owned by different institutions... ukiyo-e.org/
Search by image functionaltity
(see also http://tineye.com/)

Cat: most popular/valuable object
what are the criteria for this?
e
popularity determined by demand and number of photographies of it
MRAH is itself unknown, compared to AUTOworld ;)
they have tried to bring the hidden gems of the collection to the attention of visitors, but it was unsuccessful
visitors with the wave sweater
"it's not written there, but I'm pretty sure it's the Bill Clinton one [saxophone]"

on their website "the piece of the month" : 
Masterpiece of the month: http://www.kmkg-mrah.be/node/4079
http://www.kmkg-mrah.be/fr/node/4075
check the book "Masterpieces of the Museum" : http://www.kmkg-mrah.be/masterpieces-cinquantenaire-museum

trying to make a first page on carmentis that ties in with the exhibition they would have
challenge to link surrounding exhibition media (audio guides, games, etc) ... thinking to add this "Related objects" style tab underneath the carmentis.
Bulletins are partially public ... PDF
Item with reference (link to library site)
http://carmentis.be/eMuseumPlus?service=ExternalInterface&module=collection&objectId=91584&viewType=detailView
Currently 4 people doing "enrichment"....

Zetcom is currently developing Museum Plus RIA (rich internet application) allows for batch processing / keywords / linking with Google Maps / linking with the Getty AAT
In testing now at the museum, 
KEYWORDS! something she's really looking forward to
Creating the surrounding mateirals is what's exiciting, and this is currently blocked. Desire to make more virtual exhibtions.
Folkore collection: 70000 pieces: Not all of them would work in a physical exhibition, for instance 40000 postcards --- could be the basis of  of an online photo album, or augmented reality. But wouldn't be interesting in a traditional exhibition. Small theatrical props : could make stop motion materials. Not digitized yet (puppets)
Postcards will be digitised in spring
Patacons + Patacon moulds (example of Adam + Eve mould and result)

Dimensions of objects in the database do not (always) include the frame

Used to give courses in lace creation. They have many copies of a design in different forms. For a design drawing we might have 20 versions. We try to photograph the entire object (the entire page for instance of a drawing).
also for preservation purposes

Rapa Nui
classified under Easter island, that is located in Valpariso (Chile)
same for Nepal = classified under China ex Thang-ka
we go by what is legal at this moment; even though we try to cheat a bit by adding culture (Tibetan) ... Easter Island is part of Chile, though it's no where near.
Things made in Mesopotamia, you can't identify an actual location any more.
This was a long discussion.
Thang-ka : among most popular pieces.

Other issues: Name of Japaese print makers: "HOKU" family name ... first name, last name is more traditional
problems in transliteration. searchable on both parts of name.








