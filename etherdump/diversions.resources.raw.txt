
http://pad.constantvzw.org/p/diversions.index

DiVersions is inspired by the way versions are  inscribed in daily software-practice, and explores how parallel to their conventional narrative of collaboration and consensus they can produce divergent histories through supporting difference. What types of alternative collectivity do they make possible and  impossible? How can we use these timelines, histories, traces not just  in terms of safeguarding production, but for other ways of inscribing multiplicity and variety? http://constantvzw.org/site/Call-for-participants-DiVersions.html

Visual references: https://diversions.hotglue.me (to edit: https://diversions.hotglue.me/edit user: diversions + pw: diversions)

Collection Cinquantenaire Museum on-line http://www.carmentis.be/eMuseumPlus
Musical instrument collection included in http://www.mimo-international.com/MIMO/

Some artefacts: 
*wrong attribution http://carmentis.kmkg-mrah.be/eMuseumPlus?service=ExternalInterface&module=collection&objectId=142482
*empty http://carmentis.kmkg-mrah.be/eMuseumPlus?service=ExternalInterface&module=collection&objectId=185783&viewType=detailView
*re-naming locations (historical = colonial) http://carmentis.kmkg-mrah.be/eMuseumPlus?service=ExternalInterface&module=collection&objectId=94718&viewType=detailView

Technologies for dissensus, agonism, conflict, collaboration, divergence

"Github represents a cultural hegemony in software development today, and as such is actively displacing and distorting the practices of the very community that helped to create it." Michael Murtaugh, Thank You Github https://github.com/automatist/thank-you-github

Aymeric Mansoux, Fork Workers http://areyoubeingserved.constantvzw.org/Fork_Workers.xhtml

Carlo di Salvo, Adversarial Design http://carldisalvo.com/posts/adversarial-design/

Zach Blas & Micha Carde, “Imaginary computational systems: queer technologies and transreal aesthetics”, AI & Soc, August 2015

Le Soir édité "Le Soir édité (@lesoir_diff) est un #Twitter bot qui tente de capturer les changements et corrections d’articles publiés en ligne http://p.xuv.be/le-soir-edite

https://metacommunitiesofcode.org/

Big Diff, granularity, incoherence and production in the Github software repository (with Andrew Goffey, Adrian Mackenzie, Richard Mills and Stuart
Sharples)

http://recognition.tate.org.uk/

Troubling, queering, decolonising the digital archive

Donna Haraway, Situated Knowledges (1988) “The moral is simple: only partial perspective promises objective vision. All Western cultural narratives are allegories of the ideologies governing the relations of what we call mind and body, distance and responsibility. Feminist objectivity is about limited location and situated knowledge, not about transcendence and splitting of subject and object. It allows us to become answerable for what we learn how to see.”

“When we use the term active, it is not by contrast to an archive that would otherwise remain passive, but to indicate inter-active assemblages. No archive remains passive as such. If one looks carefully at the work of an archivist, one can see that s/he is always working on the archive for it to "remain the same" (...) Documents are kept in conditions that need constant care and attention, and even for it to remain a fixed point of reference, it changes constantly. Documents exist as 'media' forms that are subject to various processes of mediality. (...) By creating the connection between documents and excluding other connections, by contextualizing and de-contextualizing them, the archive re-writes its contents, changes the way the documents are seen/read and interpreted. There is a content-authoring inherent in any archival work. If we use the term active archive to describe our activities, it is meant more to emphasize processes that are inherent to the archive and to push its actions to the extreme. If an archive is in permanent state of mediality, always in temporality, always re-writing itself, an active active is an attempt to describe strategies and tools that amplify and diversify this process, to reveal medialities - indeed we would stress the archive as a software-machine: as readable, writable and executable. And therefore the material is provided with the ability to 'speak' for-itself."
http://activearchives.org/wiki/Archiving_the_Data-body:_human_and_nonhuman_agency_in_the_documents_of_Kurenniemi

http://www.fundaciotapies.org/blogs/prototips/

Geraldine Juárez, A pre-emptive history of Google Cultural Institute http://www.mondotheque.be/wiki/index.php/A_Pre-emptive_History_of_the_Google_Cultural_Institute
Geraldine Juárez, Intercolonial Technogalactic http://geraldine.juarez.se/publications/Intercolonial.pdf

A Queer Glossary https://vanabbemuseum.nl/fileadmin/user_upload/Queer_GLossary_2016_print_spreads.pdf

"Today Internet technology, combined with rapid moves made on the  geopolitical chessboard, make archives a contested site of affirmation, recognition and denial. As such, it is of great importance to be aware of processes of colonialisation and decolonisation taking place as new technology can both be used to affirm existing hegemonic colonial  relationships or break them open."
http://www.internationaleonline.org/bookshelves/decolonising_archives

https://interactive.quipu-project.com/#/en/quipu/intro
http://one.usc.edu/

"The opposite of archiving for so many reasons. For, ordinary life can be and is made into art, not artifacts, sometimes by loving communities in the living of it, and sometimes by later communities in the lasting of it. Archive’s opposite because the Fae Richards Archive was a carefully, lovingly researched and rendered fake, made by many, because we believed in the telling of the story of someone (Fae Richards, the Watermelon Woman) who must have been true but couldn’t benefit in her time from today’s most obvious, irresistible right and activity: living a life available to the photographic record and its lasting home in an archive. The opposite of archiving because, thankfully, real people, our lived lives and luscious loves, our full-tilt embrace of experience in community and history and art, will never be fully available to any archive’s or the internet’s quest for total picture control. Rather, we enter ourselves into history and its many archives here again, and as Zoe does and has done before, by celebrating the photograph’s partial, artistic, personal hold on people, truth, and life."
https://aljean.wordpress.com/2016/10/03/the-opposite-of-archiving-zoe-leonard-fae-richards-and-the-watermelon-woman/ 

“Images depicting former colonies have shaped and continue to inform  interpretations of the past. Libraries, museums, archives all collect  images, but also communicate them in various ways, taking part in the constant construction of colonial visual history.”
http://livingarchives.mah.se/2016/06/troublesome-pictures-seminar-02-june-video-documentation/

Decolonising, queering, troubling the museum

Queering the Van Abbe Museum (Eindhoven)
https://vanabbemuseum.nl/en/collection/queering/

"If the museum was invented as a colonial technology capable of unifying  the historical narrative, and as a collective memory prosthesis trying  to re-write the past and prefigure the future in order to legitimise its  hegemony, is it then possible to conceive a decolonial use of the  museum? Is it possible to produce a knowledge capable of explaining the historical assemblages of subjects subalternised by colonisation?" Paul B. Preciado, http://www.macba.cat/en/decolonising-museum

"'decolonising’ means both resisting the reproduction of colonial taxonomies, while simultaneously vindicating radical multiplicity. These are two forces drawing in different directions: understanding the situation museums are in, critically and openly, and identifying those moments that already indicate a different type of practice that overcomes or resists the colonial conditioning."
http://www.internationaleonline.org/bookshelves/decolonising_museums

Decolonize this space (New York)
http://www.culturestrike.org/magazine/decolonization-day
http://www.decolonizethisplace.org/

*(added by geraldine)
Decolonization is not a metaphor - Eve Tuck, K. Wayne Yang
http://decolonization.org/index.php/des/article/view/18630
The easy adoption of decolonizing discourse by educational advocacy and scholarship, evidenced by the increasing number of calls to “decolonize  our schools,” or use “decolonizing methods,” or, “decolonize student  thinking”, turns decolonization into a metaphor. As important as their  goals may be, social justice, critical methodologies, or approaches that  decenter settler perspectives have objectives that may be  incommensurable with decolonization.
Geraldine Juárez  http://para.archives.nu 

Archiving miscellaneous

"Collection Storage Tips & Tricks” (...) showcases the creative and cost-effective ideas that  collections professionals from around the world have developed to store  various types of cultural objects and collections."
http://re-org.tumblr.com/

